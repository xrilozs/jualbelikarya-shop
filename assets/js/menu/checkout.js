$('#payment-cc-form').submit(function(e){
  e.preventDefault()

  let $form     = $(this)
  let formData  = new FormData()
  formData.append("subtotal", $form.find( "input[name='subtotal']" ).val())
  formData.append("ongkir", $form.find( "input[name='ongkir']" ).val())
  formData.append("diskon", $form.find( "input[name='diskon']" ).val())
  formData.append("ppn", $form.find( "input[name='ppn']" ).val())
  formData.append("total", $form.find( "input[name='total']" ).val())

  $.ajax({
    async: true,
    url: `${BASE_URL}/checkout/payment`,
    type: 'POST',
    data: formData,
    processData: false,
    contentType: false,
    success: function(res) {
      let response = JSON.parse(res)
      if(response.status == 'failed'){
        let alert = `<div class="alert alert-danger alert-message" role="alert">Gagal melakukan pembayaran</div>`
        $('#cc-alert').html(alert)
      }else{
        $('#id_order').val(response.data)
        $('#transaction').val('success')
        $('#pembayaran-btn').removeClass("btn-danger")
        $('#pembayaran-btn').addClass("btn-success")
        $('#payment').modal('hide')
      }
    }
  });
})

$('#payment-mobile-form').submit(function(e){
  e.preventDefault()

  let $form = $(this)
  let formData  = new FormData()
  formData.append("subtotal", $form.find( "input[name='subtotal']" ).val())
  formData.append("ongkir", $form.find( "input[name='ongkir']" ).val())
  formData.append("diskon", $form.find( "input[name='diskon']" ).val())
  formData.append("ppn", $form.find( "input[name='ppn']" ).val())
  formData.append("total", $form.find( "input[name='total']" ).val())
  // let req = {
  //   "subtotal": $form.find( "input[name='subtotal']" ).val(),
  //   "ongkir": $form.find( "input[name='ongkir']" ).val(),
  //   "diskon": $form.find( "input[name='diskon']" ).val(),
  //   "ppn": $form.find( "input[name='ppn']" ).val(),
  //   "total": $form.find( "input[name='total']" ).val()
  // }
  // console.log(req)

  $.ajax({
    async: true,
    url: `${BASE_URL}/checkout/payment`,
    type: 'POST',
    data: formData,
    processData: false,
    contentType: false,
    error: function(jqXHR, textStatus, errorThrown) {
      console.log(textStatus)
    },
    success: function(data, textStatus, jqXHR) {
      let response = JSON.parse(data)
      if(response.status == 'failed'){
        let alert = `<div class="alert alert-danger alert-message" role="alert">Gagal melakukan pembayaran</div>`
        $('#mobile-alert').html(alert)
      }else{
        $('#id_order').val(response.data)
        $('#transaction').val('success')
        $('#pembayaran-btn').removeClass("btn-danger")
        $('#pembayaran-btn').addClass("btn-success")
        $('#payment').modal('hide')
      }
    }
  });
})