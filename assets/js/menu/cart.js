$('#checkout-btn').click(function(){
  console.log("CHECKOUT")
  const jml_beli_class = $('.jml_beli')
  let items = []
  $.each(jml_beli_class, function (index, item) {
    items.push( {jml_beli: $(item).val(), id_cart: $(item).data('id')} );  
  });
  const req = {
    items
  }
  console.log("REQ", req)


  $.ajax({
    async: true,
    url: `${BASE_URL}/checkout/checkout_process`,
    type: 'POST',
    data: JSON.stringify(req),
    error: function(res) {
      // const response = JSON.parse(res.responseText)
      window.location.href = `${BASE_URL}/menu/cart`
    },
    success: function(res) {
      window.location.href = `${BASE_URL}checkout`
    }
  });
})