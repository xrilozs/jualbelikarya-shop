-- Adminer 4.8.0 MySQL 8.0.30-0ubuntu0.20.04.2 dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

SET NAMES latin1;

DROP TABLE IF EXISTS `cart`;
CREATE TABLE `cart` (
  `tgl_input` datetime NOT NULL,
  `email` varchar(128) NOT NULL,
  `id_produk` int NOT NULL,
  `nama_produk` varchar(512) NOT NULL,
  `image_produk` varchar(256) NOT NULL,
  `jml_beli` int NOT NULL DEFAULT '1',
  `harga` bigint NOT NULL,
  `id_cart` bigint NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id_cart`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;


DROP TABLE IF EXISTS `complaint`;
CREATE TABLE `complaint` (
  `nama` varchar(128) NOT NULL,
  `email` varchar(128) NOT NULL,
  `no_hp` varchar(15) NOT NULL,
  `isi` text NOT NULL,
  `id_order` int NOT NULL,
  `id_complaint` int NOT NULL AUTO_INCREMENT,
  `lampiran` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id_complaint`),
  KEY `id_order` (`id_order`),
  CONSTRAINT `complaint_ibfk_1` FOREIGN KEY (`id_order`) REFERENCES `order` (`id_order`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;


DROP TABLE IF EXISTS `genre`;
CREATE TABLE `genre` (
  `id` int NOT NULL,
  `nama_aliran` varchar(15) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

INSERT INTO `genre` (`id`, `nama_aliran`) VALUES
(1,	'Abstraksi'),
(2,	'Ekspresionisme'),
(3,	'Impresionisme'),
(4,	'Kubisme'),
(5,	'Naturalisme'),
(6,	'Romantisme');

DROP TABLE IF EXISTS `object`;
CREATE TABLE `object` (
  `id` int NOT NULL,
  `nama_objek` varchar(15) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

INSERT INTO `object` (`id`, `nama_objek`) VALUES
(1,	'Hewan'),
(2,	'Manusia'),
(3,	'Tumbuhan');

DROP TABLE IF EXISTS `order`;
CREATE TABLE `order` (
  `id_order` int NOT NULL AUTO_INCREMENT,
  `tgl_order` datetime NOT NULL,
  `nama` varchar(128) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `email` varchar(128) NOT NULL,
  `no_hp` varchar(15) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `orders` varchar(5000) NOT NULL,
  `alamat` text CHARACTER SET latin1 COLLATE latin1_swedish_ci,
  `catatan` text CHARACTER SET latin1 COLLATE latin1_swedish_ci,
  `total` bigint NOT NULL,
  `negara` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `kota` varchar(100) DEFAULT NULL,
  `ppn` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `diskon` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `ongkir` bigint NOT NULL,
  `subtotal` bigint NOT NULL,
  `transaction` enum('success','failed') NOT NULL,
  `status` enum('ongoing','done') CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL DEFAULT 'ongoing',
  PRIMARY KEY (`id_order`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;


DROP TABLE IF EXISTS `produk`;
CREATE TABLE `produk` (
  `id_produk` int NOT NULL AUTO_INCREMENT,
  `nama_produk` varchar(512) NOT NULL,
  `image_produk` varchar(256) NOT NULL,
  `harga` bigint NOT NULL,
  `deskripsi` varchar(512) NOT NULL,
  `nama_aliran` varchar(15) NOT NULL,
  `nama_objek` varchar(15) NOT NULL,
  PRIMARY KEY (`id_produk`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

INSERT INTO `produk` (`id_produk`, `nama_produk`, `image_produk`, `harga`, `deskripsi`, `nama_aliran`, `nama_objek`) VALUES
(1,	'Symphony Of Birds',	'Symphony of birds.jpg',	25000000,	'Lukisan dengan latar burung yang bertengger di ranting pohon',	'Kubisme',	'Hewan'),
(2,	'The Faded Flower',	'The faded flower.jpg',	18500000,	'Lukisan dengan objek bunga yg memudar',	'Kubisme',	'Tumbuhan'),
(3,	'Inner Beauty Creature\'s',	'Inner beauty creature.jpg',	15000000,	'Lukisan yang menggambarkan kecantikan seorang wanita',	'Abstraksi',	'Manusia'),
(4,	'Vintage Garden\'s',	'Vintage garden.jpg',	10000000,	'Lukisan dengan warna vintage dan latar perkebunan',	'Kubisme',	'Tumbuhan'),
(5,	'Beauty Of Bloom Flowers',	'Beauty of bloom flower.jpg',	10000000,	'Lukisan berobjek bunga - bunga dan dedaunan',	'Kubisme',	'Tumbuhan'),
(6,	'Model\'s Expression',	'Model expression.jpg',	12500000,	'Lukisan yang menggambarkan suatu ekspresi muka',	'Abstraksi',	'Manusia'),
(7,	'Japan Bamboo\'s and The Sun',	'Japan bamboo and the sun.jpg',	7500000,	'Lukisan dengan pemandangan bambu dan matahari negara Jepang',	'Kubisme',	'Tumbuhan'),
(8,	'Innocent Bear',	'Innocent bear.jpg',	12500000,	'Lukisan seekor beruang dengan wajah yang terbagi oleh motif warna berbeda',	'Kubisme',	'Hewan'),
(10,	'test',	'user1.png',	2000000,	'test',	'',	'');

DROP TABLE IF EXISTS `role`;
CREATE TABLE `role` (
  `id` int NOT NULL AUTO_INCREMENT,
  `role` varchar(128) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

INSERT INTO `role` (`id`, `role`) VALUES
(1,	'administrator'),
(2,	'member');

DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id_user` int NOT NULL AUTO_INCREMENT,
  `nama` varchar(128) NOT NULL,
  `email` varchar(128) NOT NULL,
  `password` varchar(256) NOT NULL,
  `role_id` int NOT NULL,
  `is_active` int NOT NULL,
  PRIMARY KEY (`id_user`),
  UNIQUE KEY `email` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;


-- 2023-09-24 11:17:38
