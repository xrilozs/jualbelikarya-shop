<?php
defined('BASEPATH') or exit('No direct script access allowed');

class ModelCart extends CI_Model

{
    public function getData($table)
    {
        return $this->db->get($table)->row();
    }

    public function getDataWhere($table, $where)
    {
        $this->db->where($where);
        return $this->db->get($table);
    }

    public function getOrderByLimit($table, $order, $limit)
    {
        $this->db->order_by($order, 'desc');
        $this->db->limit($limit);
        return $this->db->get($table);
    }

    public function joinOrder($where)
    {
        $this->db->select('*');
        $this->db->from('produk');
        $this->db->join('cart', 'cart.id_produk=produk.id_produk, cart.nama_produk=produk.nama_produk, cart.image_produk=produk.image_produk');
        $this->db->join('user', 'user.email=cart.email');
        $this->db->where($where);
        return $this->db->get();
    }

    public function insertData($tabel, $data = null)
    {
        $this->db->insert($tabel, $data);
    }

    public function updateData($data, $where)
    {
        $this->db->update('cart', $data, $where);
    }

    public function deleteCart($where, $table)
    {
        $this->db->where($where);
        $this->db->delete($table);
    }

    public function kosongkanData($table)
    {
        return $this->db->truncate($table);
    }

    public function createTemp()
    {
        $this->db->query('CREATE TABLE IF NOT EXISTS cart(tgl_input DATETIME, email varchar(128), id_produk int(11), nama_produk varchar(512), image_produk varchar(256), harga bigint(255))');
    }

    public function showcart($where)
    {
        return $this->db->get('cart', $where);
    }
}
