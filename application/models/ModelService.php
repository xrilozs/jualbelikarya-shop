<?php
defined('BASEPATH') or exit('No direct script access allowed');

class ModelService extends CI_Model
{
    public function getComplaint()
    {
        return $this->db->get('complaint');
    }

    public function saveComplaint($complaint = null)
    {
        $this->db->insert('complaint', $complaint);
    }
}
