<?php
defined('BASEPATH') or exit('No direct script access allowed');

class ModelOrder extends CI_Model

{
    public function getOrder()
    {
        return $this->db->get('order');
    }

    public function getDataWhere($table, $where)
    {
        $this->db->where($where);
        return $this->db->get($table);
    }

    public function getOrderByLimit($table, $order, $limit)
    {
        $this->db->order_by($order, 'desc');
        $this->db->limit($limit);
        return $this->db->get($table);
    }

    public function joinOrder($where)
    {
        $this->db->select('*');
        $this->db->from('produk');
        $this->db->join('cart', 'cart.id_produk=produk.id_produk, cart.nama_produk=produk.nama_produk, cart.image_produk=produk.image_produk');
        $this->db->join('user', 'user.email=cart.email');
        $this->db->where($where);
        return $this->db->get();
    }

    public function detailOrder($where = null)
    {
        $sql = "INSERT INTO invoice (id_booking,id_buku) SELECT booking.id_booking,temp.id_buku FROM booking, temp WHERE temp.id_user=booking.id_user AND booking.id_user='$where'";
        $this->db->query($sql);
    }

    public function insertOrder($table, $data)
    {
        $this->db->insert($table, $data);
        return $this->db->insert_id();
    }

    public function updateOrder($table, $data, $where)
    {
        $this->db->update($table, $data, $where);
    }

    public function deleteOrder($where, $table)
    {
        $this->db->where($where);
        $this->db->delete($table);
    }

    public function find($where)
    {
        //Query mencari record berdasarkan ID-nya
        $this->db->limit(1);
        return $this->db->get('order', $where);
    }

    public function kosongkanData($table)
    {
        return $this->db->truncate($table);
    }

    public function createTemp()
    {
        $this->db->query('CREATE TABLE IF NOT EXISTS cart(tgl_input DATETIME, email varchar(128), id_produk int(11), nama_produk varchar(512), image_produk varchar(256), harga bigint(255))');
    }

    public function showOrder($where)
    {
        return $this->db->get('order', $where);
    }

    /* public function kodeOtomatis($tabel, $key)
        $this->db->select('right(' . $key . ',3) as kode', false);
        $this->db->order_by($key, 'desc');
        $this->db->limit(1);
        $query = $this->db->get($tabel);
        if ($query->num_rows() <> 0) {
            $data = $query->row();
            $kode = intval($data->kode) + 1;
        } else {
            $kode = 1;
        }
        $kodemax = str_pad($kode, 3, "0", STR_PAD_LEFT);
        $kodejadi = date('dmY') . $kodemax;
        return $kodejadi;
    */
}
