<?php
defined('BASEPATH') or exit('No direct script access allowed');

class ModelCategory extends CI_Model
{
    public function getGenre()
    {
        return $this->db->get('genre');
    }

    public function getObject()
    {
        return $this->db->get('object');
    }

    public function getCategoryGenre($genre_name)
    {
        return $this->db->get_where('genre', ['nama_aliran' => $genre_name])->row_array();
    }

    public function getProductByGenre($nama_aliran)
    {
        return $this->db->get_where('produk', ['nama_aliran' => $nama_aliran])->result_array();
    }

    public function getCategoryObject($object_name)
    {
        return $this->db->get_where('object', ['nama_objek' => $object_name])->row_array();
    }

    public function getProductByObject($nama_objek)
    {
        return $this->db->get_where('produk', ['nama_objek' => $nama_objek])->result_array();
    }
}
