<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Home extends CI_Controller
{
    public function index()
    {
        $data['judul'] = 'Home Page';

        if ($this->session->userdata('role_id') == '1') {
            $this->session->set_flashdata('maintenance', '<div class="alert alert-info alert-message" role="alert" style="top: 0; text-align: center; position: sticky; position: -webkit-sticky; z-index: 50;">Kembali ke menu admin? <a href="admin">Iya</a></div>');
            $this->load->view('templates/menuheader', $data);
            $this->load->view('templates/menutopbar');
            $this->load->view('menu/home');
            $this->load->view('templates/menufooter');
        } else {
            $this->load->view('templates/menuheader', $data);
            $this->load->view('templates/menutopbar');
            $this->load->view('menu/home');
            $this->load->view('templates/menufooter');
        }
    }
}
