<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Cart extends CI_Controller
{
    public function index()
    {
        if ($this->session->userdata('role_id') == '1') {
            redirect('home');
        } else {
            $this->session->set_flashdata('pesan', '<div class="alert alert-danger alert-message" role="alert">Anda belum login, silahkan login terlebih dahulu 😅</div>');
            redirect('login');
        }
    }

    public function delete()
    {
        $id_produk = $this->uri->segment(3);

        $this->ModelCart->deleteCart(['id_produk' => $id_produk], 'cart');
        redirect(base_url() . 'menu/cart');
    }
}
