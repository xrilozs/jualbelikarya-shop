<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Member extends CI_Controller
{
    public function logout()
    {
        if ($this->session->userdata('role_id') != '2') {
            redirect();
        } else {
            $this->session->unset_userdata('email');
            $this->session->unset_userdata('role_id');

            $this->session->set_flashdata('pesan', '<div class="alert alert-warning" role="alert" style="text-align: center;">Kamu telah melakukan logout!<button data-dismiss="alert" class="close" type="button">×</button></div>');
            redirect('login');
        }
    }
}
