<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Categories extends CI_Controller
{
    public function index()
    {
        $data = [
            'judul' => 'Categories',
            'produk' => $this->ModelProduct->getProduk()->result_array(),
            'genre' => $this->ModelCategory->getGenre()->result_array(),
            'object' => $this->ModelCategory->getObject()->result_array(),
        ];

        if ($this->session->userdata('role_id') == '1') {
            $this->session->set_flashdata('maintenance', '<div class="alert alert-info alert-message" role="alert" style="top: 0; text-align: center; position: sticky; position: -webkit-sticky; z-index: 50;">Kembali ke menu admin? <a href="admin">Iya</a></div>');
            $this->load->view('templates/menuheader', $data);
            $this->load->view('templates/menutopbar');
            $this->load->view('menu/categories', $data);
            $this->load->view('templates/menufooter');
        } else {
            $this->load->view('templates/menuheader', $data);
            $this->load->view('templates/menutopbar');
            $this->load->view('menu/categories', $data);
            $this->load->view('templates/menufooter');
        }
    }

    public function genre()
    {
        $nama_aliran = $this->input->get('filter');
        $aliran = $this->ModelCategory->getCategoryGenre($nama_aliran);
        $data = [
            'judul' => 'Categories',
            'produk' => $this->ModelCategory->getProductByGenre($aliran['nama_aliran']),
            'genre' => $this->ModelCategory->getGenre()->result_array(),
            'object' => $this->ModelCategory->getObject()->result_array(),
        ];

        if ($this->session->userdata('role_id') == '1') {
            $this->session->set_flashdata('maintenance', '<div class="alert alert-info alert-message" role="alert" style="top: 0; text-align: center; position: sticky; position: -webkit-sticky; z-index: 50;">Kembali ke menu admin? <a href="admin">Iya</a></div>');
            $this->load->view('templates/menuheader', $data);
            $this->load->view('templates/menutopbar');
            $this->load->view('menu/categories', $data);
            $this->load->view('templates/menufooter');
        } else {
            $this->load->view('templates/menuheader', $data);
            $this->load->view('templates/menutopbar');
            $this->load->view('menu/categories', $data);
            $this->load->view('templates/menufooter');
        }
    }

    public function object()
    {
        $nama_object = $this->input->get('filter');
        $objek = $this->ModelCategory->getCategoryObject($nama_object);
        $data = [
            'judul' => 'Categories',
            'produk' => $this->ModelCategory->getProductByObject($objek['nama_objek']),
            'genre' => $this->ModelCategory->getGenre()->result_array(),
            'object' => $this->ModelCategory->getObject()->result_array(),
        ];

        if ($this->session->userdata('role_id') == '1') {
            $this->session->set_flashdata('maintenance', '<div class="alert alert-info alert-message" role="alert" style="top: 0; text-align: center; position: sticky; position: -webkit-sticky; z-index: 50;">Kembali ke menu admin? <a href="admin">Iya</a></div>');
            $this->load->view('templates/menuheader', $data);
            $this->load->view('templates/menutopbar');
            $this->load->view('menu/categories', $data);
            $this->load->view('templates/menufooter');
        } else {
            $this->load->view('templates/menuheader', $data);
            $this->load->view('templates/menutopbar');
            $this->load->view('menu/categories', $data);
            $this->load->view('templates/menufooter');
        }
    }
}
