<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Product extends CI_Controller
{
    public function detail()
    {
        $id_produk = $this->uri->segment(3);
        $product = $this->ModelProduct->detailProduk(['produk.id_produk' => $id_produk])->result();
        $data['judul'] = "Detail Produk";

        foreach ($product as $fields) {
            $data['id_produk'] = $id_produk;
            $data['nama_produk'] = $fields->nama_produk;
            $data['image_produk'] = $fields->image_produk;
            $data['harga'] = $fields->harga;
            $data['deskripsi'] = $fields->deskripsi;
            $data['aliran'] = $fields->nama_aliran;
            $data['objek'] = $fields->nama_objek;
        }
        $this->load->view('templates/menuheader', $data);
        $this->load->view('templates/menutopbar');
        $this->load->view('detail/product', $data);
        $this->load->view('templates/menufooter');
    }

    public function genre()
    {
        $nama_aliran = $this->input->get('filter');
        $aliran = $this->ModelCategory->getCategoryGenre($nama_aliran);
        $data = [
            'judul' => 'Categories',
            'user' => $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array(),
            'produk' => $this->ModelCategory->getProductByGenre($aliran['nama_aliran']),
            'genre' => $this->ModelCategory->getGenre()->result_array(),
            'object' => $this->ModelCategory->getObject()->result_array(),
        ];

        $this->load->view('templates/menuheader', $data);
        $this->load->view('templates/menutopbaruser', $data);
        $this->load->view('menu/categoriesuser', $data);
        $this->load->view('templates/menufooteruser');
    }

    public function object()
    {
        $nama_object = $this->input->get('filter');
        $objek = $this->ModelCategory->getCategoryObject($nama_object);
        $data = [
            'judul' => 'Categories',
            'user' => $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array(),
            'produk' => $this->ModelCategory->getProductByObject($objek['nama_objek']),
            'genre' => $this->ModelCategory->getGenre()->result_array(),
            'object' => $this->ModelCategory->getObject()->result_array(),
        ];

        $this->load->view('templates/menuheader', $data);
        $this->load->view('templates/menutopbaruser', $data);
        $this->load->view('menu/categoriesuser', $data);
        $this->load->view('templates/menufooteruser');
    }
}
