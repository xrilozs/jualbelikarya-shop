<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Admin extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();

        if ($this->session->userdata('role_id') != '1') {
            $this->session->set_flashdata('pesan', '<div class="alert alert-danger alert-message" role="alert">Anda belum login, silahkan login terlebih dahulu 😅</div>');
            redirect('login');
        }
        $this->load->model('ModelProduct');
    }

    public function index()
    {
        $data['user'] = $this->ModelUser->getUser()->result_array();
        $data['user'] = $this->ModelUser->cekData(['email' => $this->session->userdata('email')])->row_array();
        $data['complaint'] = $this->ModelService->getComplaint()->result_array();

        $this->load->view('templates/headeradmin');
        $this->load->view('templates/topbaradmin', $data);
        $this->load->view('dashboard/admin', $data);
        $this->load->view('templates/footeradmin');
    }

    public function dashboard()
    {
        $data['user'] = $this->ModelUser->getUser()->result_array();
        $data['user'] = $this->ModelUser->cekData(['email' => $this->session->userdata('email')])->row_array();
        $data['complaint'] = $this->ModelService->getComplaint()->result_array();

        $this->load->view('templates/headeradmin');
        $this->load->view('templates/topbaradmin', $data);
        $this->load->view('dashboard/admin', $data);
        $this->load->view('templates/footeradmin');
    }

    public function orders()
    {
        $data['order'] = $this->ModelOrder->getOrder()->result_array();

        $this->load->view('templates/headeradmin');
        $this->load->view('templates/topbaradmin');
        $this->load->view('dashboard/orders', $data);
        $this->load->view('templates/footeradmin');
    }

    public function members()
    {
        $data['user'] = $this->ModelUser->getUser()->result_array();

        $this->load->view('templates/headeradmin');
        $this->load->view('templates/topbaradmin');
        $this->load->view('dashboard/members', $data);
        $this->load->view('templates/footeradmin');
    }

    public function products()
    {
        $data['produk'] = $this->ModelProduct->getProduk()->result_array();

        $this->load->view('templates/headeradmin');
        $this->load->view('templates/topbaradmin');
        $this->load->view('dashboard/products', $data);
        $this->load->view('templates/footeradmin');
    }

    public function complaints()
    {
        $data['complaint'] = $this->ModelService->getComplaint()->result_array();

        $this->load->view('templates/headeradmin');
        $this->load->view('templates/topbaradmin');
        $this->load->view('dashboard/complaints', $data);
        $this->load->view('templates/footeradmin');
    }

    public function tambahProduk()
    {
        $config['upload_path'] = './assets/img/upload/';
        $config['allowed_types'] = 'jpg|png|jpeg';

        $this->load->library('upload', $config);

        if ($this->upload->do_upload('image')) {
            $image = $this->upload->data();
            $gambar = $image['file_name'];
        } else {
            $gambar = $this->input->post('image');
        }

        $data = [
            'image_produk' => $gambar,
            'nama_produk' => $this->input->post('productname'),
            'harga' => $this->input->post('price'),
            'deskripsi' => $this->input->post('desc')
        ];
        $this->ModelProduct->simpanProduk($data);

        $this->session->set_flashdata('pesan', '<div class="alert alert-info alert-message" role="alert"><h4 style="text-align: center;">Produk sudah terinput ke dalam database</h4></div>');
        redirect('admin/products');
    }

    public function hapusProduk()
    {
        $where = ['id_produk' => $this->uri->segment(3)];
        $this->ModelProduct->hapusProduk($where);
        redirect('admin/products');
    }

    public function ubahProduk()
    {
        $data['product'] = $this->ModelProduct->produkWhere(['id_produk' => $this->uri->segment(3)])->result_array();

        //konfigurasi sebelum gambar diupload
        $config['upload_path'] = './assets/img/upload/';
        $config['allowed_types'] = 'jpg|png|jpeg';
        $config['max_size'] = '3000';
        $config['max_width'] = '1000';
        $config['max_height'] = '1000';
        $config['file_name'] = 'img' . time();
        //memuat atau memanggil library upload
        $this->load->library('upload', $config);
        if ($this->form_validation->run() == false) {
            $this->load->view('templates/headeradmin');
            $this->load->view('templates/topbaradmin');
            $this->load->view('dashboard/edit', $data);
            $this->load->view('templates/footeradmin');
        } else {
            if ($this->upload->do_upload('image')) {
                $image = $this->upload->data();
                unlink('assets/img/upload/' . $this->input->post('old_pict', TRUE));
                $gambar = $image['file_name'];
            } else {
                $gambar = $this->input->post('old_pict', TRUE);
            }
            $data = [
                'nama_produk' => $this->input->post('nama_produk', true),
                'image_produk' => $gambar,
                'harga' => $this->input->post('harga', true),
                'deskripsi' => $this->input->post('deskripsi', true),
                'objek' => $this->input->post('objek', true),
            ];
            $this->ModelProduct->updateproduk($data, ['id_produk' => $this->input->post('id_produk')]);
            $this->session->set_flashdata('pesan', '<div class="alert alert-success alert-message" role="alert"><h4 style="text-align: center;">Produk sudah diubah</h4></div>');
            redirect('admin/products');
        }
    }

    public function logout()
    {
        if ($this->session->userdata('role_id') != '1') {
            redirect();
        } else {
            $this->session->unset_userdata('email');
            $this->session->unset_userdata('role_id');

            $this->session->set_flashdata('pesan', '<div class="alert alert-warning" role="alert" style="text-align: center;">Kamu telah melakukan logout!<button data-dismiss="alert" class="close" type="button">×</button></div>');
            redirect('login');
        }
    }
}
