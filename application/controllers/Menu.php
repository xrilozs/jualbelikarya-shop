<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Menu extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('ModelProduct', 'ModelCart');
    }

    public function home()
    {
        if ($this->session->userdata('role_id') == '2') {
            $data = [
                'judul' => 'Home Page',
                'user' => $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array(),
            ];

            $this->load->view('templates/menuheader', $data);
            $this->load->view('templates/menutopbaruser', $data);
            $this->load->view('menu/home');
            $this->load->view('templates/menufooteruser');
        } else {
            redirect('home');
        }
    }

    public function about()
    {
        if ($this->session->userdata('role_id') == '2') {
            $data = [
                'judul' => 'About Us',
                'user' => $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array(),
            ];

            $this->load->view('templates/menuheader', $data);
            $this->load->view('templates/menutopbaruser', $data);
            $this->load->view('menu/about');
            $this->load->view('templates/menufooteruser');
        } else {
            redirect('about');
        }
    }

    public function categories()
    {
        if ($this->session->userdata('role_id') == '2') {
            $data = [
                'judul' => 'Categories',
                'user' => $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array(),
                'produk' => $this->ModelProduct->getProduk()->result_array(),
                'genre' => $this->ModelCategory->getGenre()->result_array(),
                'object' => $this->ModelCategory->getObject()->result_array(),
            ];

            $this->load->view('templates/menuheader', $data);
            $this->load->view('templates/menutopbaruser', $data);
            $this->load->view('menu/categoriesuser', $data);
            $this->load->view('templates/menufooteruser');
        } else {
            redirect('categories');
        }
    }

    public function cart()
    {
        if ($this->session->userdata('role_id') == '2') {
            $email = $this->session->userdata('email');
            $data = [
                'js' => base_url('assets/js/menu/cart.js'),
                'judul' => 'Shopping Cart',
                'user' => $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array(),
                'cart' => $this->db->query("select id_cart, id_produk, nama_produk, image_produk, jml_beli, harga from cart where email='$email'")->result_array(),
            ];

            $this->ModelCart->showcart(['email' => $email])->num_rows();

            $this->load->view('templates/menuheader', $data);
            $this->load->view('templates/menutopbaruser', $data);
            $this->load->view('menu/cart', $data);
            $this->load->view('templates/menufooteruser', $data);
        } else {
            redirect('cart');
        }
    }

    public function detailproduk()
    {
        $id_produk = $this->uri->segment(3);
        $product = $this->ModelProduct->detailProduk(['produk.id_produk' => $id_produk])->result();
        $data = [
            'judul' => "Product Details",
            'user' => $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array()
        ];

        foreach ($product as $fields) {
            $data['id_produk'] = $id_produk;
            $data['nama_produk'] = $fields->nama_produk;
            $data['image_produk'] = $fields->image_produk;
            $data['harga'] = $fields->harga;
            $data['deskripsi'] = $fields->deskripsi;
            $data['objek'] = $fields->objek;
        }
        $this->load->view('templates/menuheader', $data);
        $this->load->view('templates/menutopbaruser', $data);
        $this->load->view('detail/product', $data);
        $this->load->view('templates/menufooteruser');
    }

    public function tambahproduk()
    {
        if ($this->session->userdata('role_id') == '2') {
            $id_produk = $this->uri->segment(3);
            $p = $this->db->query("select * from produk where id_produk='$id_produk'")->row();

            $isi = [
                'tgl_input' => date('Y-m-d H:i:s'),
                'email' => $this->session->userdata('email'),
                'id_produk' => $id_produk,
                'nama_produk' => $p->nama_produk,
                'image_produk' => $p->image_produk,
                'harga' => $p->harga,
            ];
            $this->ModelCart->insertData('cart', $isi);
            redirect(base_url() . 'menu/categories');
        } else {
            $this->session->set_flashdata('pesan', '<div class="alert alert-danger alert-message" role="alert">Anda belum login, silahkan login terlebih dahulu 😅</div>');
            redirect('login');
        }
    }
}
