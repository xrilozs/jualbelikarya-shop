<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Service extends CI_Controller
{
    public function help()
    {
        $data = [
            'judul' => 'Pusat Bantuan',
            'user' => $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array(),
        ];

        if ($this->session->userdata('role_id') == '2') {
            $this->load->view('templates/menuheader', $data);
            $this->load->view('templates/menutopbaruser', $data);
            $this->load->view('service/help');
            $this->load->view('templates/menufooteruser');
        } else {
            $this->load->view('templates/menuheader', $data);
            $this->load->view('templates/menutopbar');
            $this->load->view('service/help');
            $this->load->view('templates/menufooter');
        }
    }

    public function complaint()
    {
        $data = [
            'judul' => 'Layanan Pengaduan',
            'user' => $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array(),
            'order' => $this->db->get_where('order', ['email' => $this->session->userdata('email')])->result_array(),
        ];

        if ($this->session->userdata('role_id') == '2') {
            $this->load->view('templates/menuheader', $data);
            $this->load->view('templates/menutopbaruser', $data);
            $this->load->view('service/complaint', $data);
            $this->load->view('templates/menufooteruser');
        } else {
            redirect('home');
        }
    }

    public function add_complaint()
    {
        $config['upload_path'] = './assets/pengaduan/';
        $config['allowed_types'] = 'jpg|png|jpeg|mp4|3gp|mov|webm';
        $this->load->library('upload', $config);

        $lampiran = null;
        if ($this->upload->do_upload('attach')) {
            $lampiran_res   = $this->upload->data();
            $lampiran       = $lampiran_res['file_name'];
        }

        $complaint = [
            'nama'      => $this->input->post('nama'),
            'email'     => $this->input->post('email'),
            'no_hp'     => $this->input->post('no_hp'),
            'isi'       => $this->input->post('complaint'),
            'id_order'  => $this->input->post('id_order'),
            'lampiran'  => $lampiran
        ];
        $this->ModelService->saveComplaint($complaint);
        $this->session->set_flashdata('pesan', '<div class="alert alert-success alert-message" role="alert">Pengaduan Anda telah terkirim</div>');
        redirect('service/complaint');
    }

    public function terms()
    {
        $data = [
            'judul' => 'Syarat & Ketentuan',
            'user' => $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array(),
        ];

        if ($this->session->userdata('role_id') == '2') {
            $this->load->view('templates/menuheader', $data);
            $this->load->view('templates/menutopbaruser', $data);
            $this->load->view('terms');
            $this->load->view('templates/menufooteruser');
        } else {
            $this->load->view('templates/menuheader', $data);
            $this->load->view('templates/menutopbar');
            $this->load->view('terms');
            $this->load->view('templates/menufooter');
        }
    }

    public function conditions()
    {
        $data['judul'] = 'Syarat & Ketentuan';

        $this->load->view('templates/menuheader', $data);
        $this->load->view('templates/menutopbar');
        $this->load->view('conditions');
        $this->load->view('templates/menufooter');
    }
}
