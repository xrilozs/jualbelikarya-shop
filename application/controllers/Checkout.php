<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Checkout extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();

        if ($this->session->userdata('role_id') != '2') {
            $this->session->set_flashdata('pesan', '<div class="alert alert-danger alert-message" role="alert">Anda belum login, silahkan login terlebih dahulu 😅</div>');
            redirect('login');
        }
    }

    public function index()
    {
        if ($this->session->userdata('role_id') == '2') {
            $email = $this->session->userdata('email');
            $data = [
                'js' => base_url('assets/js/menu/checkout.js'),
                'judul' => 'Checkout Order',
                'user'  => $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array(),
                'cart'  => $this->db->query("select id_produk, nama_produk, image_produk, harga, jml_beli from cart where email='$email'")->result_array(),
                'order' => $this->db->get_where('order', ['email' => $this->session->userdata('email'), 'status' => 'ongoing'])->row_array(),
            ];
            $barang = $this->db->query("select * from cart where email='$email'")->num_rows();

            if ($barang < 1) {
                echo "<script type='text/javascript'>alert('Maaf, tidak ada barang pada troli. Mohon tambahkan barang terlebih dahulu. 😅'); window.location.href='menu/cart';</script>";
            } else {
                $this->ModelCart->showcart(['email' => $email])->num_rows();

                $this->load->view('templates/menuheader', $data);
                $this->load->view('templates/menutopbaruser', $data);
                $this->load->view('menu/checkout', $data);
                $this->load->view('templates/menufooteruser');
            }
        } else {
            redirect('home');
        }
    }

    public function checkout_process(){
        if ($this->session->userdata('role_id') == '2') {
            $request = json_decode($this->input->raw_input_stream, true);
            $items = $request['items'];
            foreach ($items as $item) {
                $this->ModelCart->updateData($item, array("id_cart" => $item['id_cart']));
            }
            echo "{status: 'success'}";
        } else {
            redirect('home');
        }
    }

    public function proses_pembayaran()
    {
        if ($this->input->post('konfirmasi_pembayaran')) {
            $status = 'success';
        } elseif ($this->input->post('konfirmasi_pembayaran')) {
            $status = 'pending';
        } else {
            $status = 'failed';
        }
        $data['status'] = $status;
        $this->load->view('checkout/index', $data);
    }

    public function payment()
    {
        $email = $this->session->userdata('email');
        $order = $this->db->query("select nama_produk, harga, jml_beli from cart where email='$email'")->result_array();

        $orders = json_encode($order);
        $orderbrg = array(
            'tgl_order'     => date('Y-m-d H:i:s'),
            'email'         => $email,
            'orders'        => $orders,
            'subtotal'      => $this->input->post('subtotal'),
            'ppn'           => $this->input->post('ppn'),
            'diskon'        => $this->input->post('diskon'),
            'ongkir'        => $this->input->post('ongkir'),
            'total'         => $this->input->post('total'),
            'transaction'   => 'success',
            'status'        => 'ongoing'
        );

        //menyimpan data order ke tabel order dan invoice, dan mengosongkan tabel cart
        $order_id = $this->ModelOrder->insertOrder('order', $orderbrg);
        if($order_id){
            echo json_encode(array(
                "status" => "success",
                "data"   => $order_id
            ));
        }else{
            echo json_encode(array(
                "status" => "failed"
            ));
        }
    }

    public function order()
    {
        $email      = $this->session->userdata('email');
        $order      = $this->db->query("select nama_produk, harga, jml_beli from cart where email='$email'")->result_array();
        $id_order   = $this->input->post('id_order');

        $orders     = json_encode($order);
        $orderbrg   = array(
            'tgl_order'     => date('Y-m-d H:i:s'),
            'nama'          => $this->input->post('NamaLengkap'),
            'email'         => $email,
            'no_hp'         => $this->input->post('nohp'),
            'orders'        => $orders,
            'negara'        => $this->input->post('negara'),
            'kota'          => $this->input->post('kota'),
            'alamat'        => $this->input->post('alamat'),
            'catatan'       => $this->input->post('catatan'),
            'subtotal'      => $this->input->post('subtotal'),
            'ppn'           => $this->input->post('ppn'),
            'diskon'        => $this->input->post('diskon'),
            'ongkir'        => $this->input->post('ongkir'),
            'total'         => $this->input->post('total'),
            'transaction'   => $this->input->post('transaction'),
            'status'        => 'done'
        );

        if($id_order){
            $this->ModelOrder->updateOrder('order', $orderbrg, array("id_order" => $id_order));
        }else{
            $id_order = $this->ModelOrder->insertOrder('order', $orderbrg);
        }

        //menyimpan data order ke tabel order dan invoice, dan mengosongkan tabel cart
        $this->ModelCart->deleteCart(['email' => $email], 'cart');
        redirect(base_url() . 'checkout/invoice/'.$id_order);
    }

    public function invoice($id)
    {
        if ($this->session->userdata('role_id') == '2') {
            $email = $this->session->userdata('email');
            $data = [
                'order' => $this->db->query("select * from `order` where email='$email' and id_order=$id")->row_array(),
            ];
            $this->load->view('invoice', $data);
        } else {
            redirect('home');
        }
    }
}
