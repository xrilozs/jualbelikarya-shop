<div class="page-holder">
    <!-- navbar-->
    <header class="header bg-white">
        <div class="container px-0 px-lg-3">
            <nav class="navbar navbar-expand-lg navbar-light py-3 px-lg-0"><a class="navbar-brand"><span class="font-weight-bold text-uppercase text-dark">Jual Beli Karya</span></a>
                <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation"><span class="navbar-toggler-icon"></span></button>
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav ml-auto">
                        <li class="nav-item"><a class="nav-link" href="<?= base_url('menu/cart'); ?>"> <i class="fas fa-dolly-flatbed mr-1 text-gray"></i>Troli Belanja<small class="text-gray"> (0)</small></a></li>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" id="pagesDropdown" href="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fas fa-user-alt mr-1 text-gray"></i><?= $user['nama']; ?></a>
                            <div class="dropdown-menu mt-3" aria-labelledby="pagesDropdown"><a class="dropdown-item border-0 transition-link" href="<?= base_url('login/logout'); ?>">Logout</a></div>
                        </li>
                    </ul>
                </div>
            </nav>
        </div>
    </header>