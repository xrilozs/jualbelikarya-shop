<!DOCTYPE html>
<html lang="en">

<head>
    <title>Admin Page</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css">
    <link rel="stylesheet" href="<?= base_url('assets/'); ?>css/admin/bootstrap.min.css">
    <link rel="stylesheet" href="<?= base_url('assets/'); ?>css/admin/bootstrap-responsive.min.css">
    <link rel="stylesheet" href="<?= base_url('assets/'); ?>css/admin/fullcalendar.css">
    <link rel="stylesheet" href="<?= base_url('assets/'); ?>css/admin/uniform.css">
    <link rel="stylesheet" href="<?= base_url('assets/'); ?>css/admin/select2.css" />
    <link rel="stylesheet" href="<?= base_url('assets/'); ?>css/admin/maruti-style.css">
    <link rel="stylesheet" href="<?= base_url('assets/'); ?>css/admin/maruti-media.css" class="skin-color">
    <!--======================================================================-->
    <link rel="shortcut icon" href="<?= base_url('assets/'); ?>icons/admin/admin_icon.png">
    <!--======================================================================-->
    <script src="https://cdn.jsdelivr.net/npm/chart.js"></script>
    <!--======================================================================-->
</head>

<body>
    <!--Header-part-->
    <div id="header">
        <h1 style="text-align: center;"><b>Admin <i style="color: #dcb14a;">JualBeliKarya</i></b></h1>
    </div>
    <!--close-Header-part-->