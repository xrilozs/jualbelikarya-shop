<!--top-Header-menu-->
<div id="user-nav" class="navbar navbar-inverse">
    <ul class="nav">
        <li class=""><a href="" title="Administrator"><i class="far fa-user"></i></a></li>
        <li class=""><a href="<?= base_url('admin/logout'); ?>"><i class="far fa-sign-out"></i> <span class="text">Logout</span></a></li>
    </ul>
</div>
<!--close-top-Header-menu-->