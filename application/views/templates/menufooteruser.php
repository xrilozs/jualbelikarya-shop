<footer class="bg-dark text-white">
    <div class="container py-3">
        <div class="row py-5">
            <div class="col-md-4 mb-3 mb-md-0">
                <h6 class="text-uppercase mb-3">Layanan Pelanggan</h6>
                <ul class="list-unstyled mb-0">
                    <li><a class="footer-link" href="<?= base_url('service/help'); ?>">Bantuan &amp; Hubungi Kami</a></li>
                    <li><a class="footer-link" href="<?= base_url('service/complaint');  ?>">Layanan Pengaduan</a></li>
                    <li><a class="footer-link" href="<?= base_url('service/terms'); ?>">Syarat &amp; Ketentuan</a></li>
                </ul>
            </div>
            <div class="col-md-4 mb-3 mb-md-0">
                <h6 class="text-uppercase mb-3">Tentang Kami</h6>
                <ul class="list-unstyled mb-0">
                    <li><a class="footer-link" href="<?= base_url('menu/about'); ?>">Profil</a></li>
                </ul>
            </div>
            <div class="col-md-4">
                <h6 class="text-uppercase mb-3">Media Sosial</h6>
                <ul class="list-unstyled mb-0">
                    <li><a class="footer-link" href=""><i class="fab fa-twitter"><b> Twitter</b></i></a></li>
                    <li><a class="footer-link" href=""><i class="fab fa-facebook"><b> Facebook</b></i></a></li>
                    <li><a class="footer-link" href=""><i class="fab fa-instagram"><b> Instagram</b></i></a></li>
                    <li><a class="footer-link" href=""><i class="fab fa-pinterest"><b> Pinterest</b></i></a></li>
                </ul>
            </div>
        </div>
        <div class="border-top pt-4 mb-3" style="border-color: #1d1d1d !important">
            <div class="row">
                <div class="col-lg text-lg-center">
                    <p class="small text-muted mb-0" style="text-align: center;"><b>Jual Beli Karya</b> &copy; 2023</p>
                </div>
            </div>
        </div>
    </div>
</footer>

<!-- JavaScript files-->
<script src="<?= base_url('assets/'); ?>vendor/menu/jquery/jquery.min.js"></script>
<script src="<?= base_url('assets/'); ?>vendor/menu/bootstrap/js/bootstrap.bundle.min.js"></script>
<script src="<?= base_url('assets/'); ?>vendor/menu/lightbox2/js/lightbox.min.js"></script>
<script src="<?= base_url('assets/'); ?>vendor/menu/nouislider/nouislider.min.js"></script>
<script src="<?= base_url('assets/'); ?>vendor/menu/bootstrap-select/js/bootstrap-select.min.js"></script>
<script src="<?= base_url('assets/'); ?>vendor/menu/owl.carousel2/owl.carousel.min.js"></script>
<script src="<?= base_url('assets/'); ?>vendor/menu/owl.carousel2.thumbs/owl.carousel2.thumbs.min.js"></script>
<script src="<?= base_url('assets/'); ?>js/menu/front.js"></script>

<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.1/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
</div>

<script>
    let BASE_URL = "<?=base_url();?>";
    $('.alert').alert().delay(2000).slideUp('slow');
</script>
<script>
    function aturan() {
        document.getElementById('syarat').style.backgroundColor = 'yellow';
    }
</script>

<!--Custom JS-->
<?php if(isset($js)): ?>
    <script src="<?= $js; ?>"></script>
<?php endif; ?>
</body>

</html>