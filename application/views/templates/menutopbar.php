<div class="page-holder">
    <!-- navbar-->
    <?= $this->session->flashdata('maintenance'); ?>
    <header class="header bg-white">
        <div class="container px-0 px-lg-3">
            <nav class="navbar navbar-expand-lg navbar-light py-3 px-lg-0">
                <div class="navbar-brand"><span class="font-weight-bold text-uppercase text-dark">Jual Beli Karya</span></div>
                <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation"><span class="navbar-toggler-icon"></span></button>
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav mr-auto">
                        <li class="nav-item">
                            <!-- Link--><a class="nav-link" href="<?= base_url('home'); ?>">Home</a>
                        </li>
                        <li class="nav-item">
                            <!-- Link--><a class="nav-link" href="<?= base_url('categories'); ?>">Kategori</a>
                        </li>
                        <li class="nav-item">
                            <!-- Link--><a class="nav-link" href="<?= base_url('about'); ?>">Tentang Kami</a>
                        </li>
                    </ul>
                    <ul class="navbar-nav ml-auto">
                        <li class="nav-item"><a class="nav-link" href="<?= base_url('cart'); ?>"> <i class="fas fa-dolly-flatbed mr-1 text-gray"></i>Troli Belanja<small class="text-gray"> (0)</small></a></li>
                        <li class="nav-item"><a class="nav-link" href="<?= base_url('login'); ?>"> <i class="fas fa-user-alt mr-1 text-gray"></i>Log Masuk</a></li>
                    </ul>
                </div>
            </nav>
        </div>
    </header>