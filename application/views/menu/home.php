<!-- HERO SECTION-->
<div class="container">
    <section class="hero pb-3 bg-cover bg-center d-flex align-items-center" style="background-image: url(https://farm3.staticflickr.com/2783/4120420946_4e1a8473a5_o.jpg);">
        <div class="container py-5">
            <div class="row px-4 px-lg-5">
                <div class="col-lg-6">
                    <p class="small text-uppercase mb-2" style="color: ghostwhite;">Spesial Tahun Baru 2023</p>
                    <h1 class="h2 text-uppercase mb-3">Diskon 20% Khusus Hari Ini</h1><a class="btn btn-dark" id="explore" href="<?= base_url('categories'); ?>">Cari Koleksi Lukisan</a>
                </div>
            </div>
        </div>
    </section>
    <!-- TRENDING PRODUCTS-->
    <section class="py-5">
        <header>
            <p class="small text-muted small text-uppercase mb-1">Lukisan Terbaik</p>
            <h2 class="h5 text-uppercase mb-4" id="highlight">Produk Terpopuler Hari Ini</h2>
        </header>
        <div class="row">
            <!-- PRODUCT 1-->
            <div class="col-xl-3 col-lg-4 col-sm-6">
                <div class="text-center">
                    <div class="position-relative mb-3">
                        <img class="img-fluid w-100" src="https://i.pinimg.com/236x/58/31/07/583107b586f3b411edfdf64a82b3c81f.jpg" alt="...">
                    </div>
                    <h6 class="reset-anchor">Beauty Of Bloom Flowers</h6>
                    <p class="small text-muted">Rp 10.000.000</p>
                </div>
            </div>
            <!-- PRODUCT 2-->
            <div class="col-xl-3 col-lg-4 col-sm-6">
                <div class="text-center">
                    <div class="position-relative mb-3">
                        <img class="img-fluid w-100" src="https://i.pinimg.com/236x/23/8c/c1/238cc159c24f0ef83db2165241509559.jpg" alt="...">
                    </div>
                    <h6 class="reset-anchor">Vintage Garden's</h6>
                    <p class="small text-muted"><s style="color: peru;"> Rp 12.500.000 </s> &nbsp; Rp 10.000.000</p>
                </div>
            </div>
            <!-- PRODUCT 3-->
            <div class="col-xl-3 col-lg-4 col-sm-6">
                <div class="text-center">
                    <div class="position-relative mb-3">
                        <img class="img-fluid w-100" src="https://i.pinimg.com/236x/b2/8d/6a/b28d6a0ce84bcf4386999159a1a19193.jpg" alt="...">
                    </div>
                    <h6 class="reset-anchor">Inner Beauty Creature's</h6>
                    <p class="small text-muted">Rp 15.000.000</p>
                </div>
            </div>
            <!-- PRODUCT 4-->
            <div class="col-xl-3 col-lg-4 col-sm-6">
                <div class="text-center">
                    <div class="position-relative mb-3">
                        <img class="img-fluid w-100" src="https://i.pinimg.com/236x/4a/19/93/4a19930fd149502d5bb296e3794317b7.jpg" alt="...">
                    </div>
                    <h6 class="reset-anchor">Hexagon Of Destiny</h6>
                    <p class="small text-muted">Rp 5.000.000</p>
                </div>
            </div>
            <!-- PRODUCT 5-->
            <div class="col-xl-3 col-lg-4 col-sm-6">
                <div class="text-center">
                    <div class="position-relative mb-3">
                        <img class="img-fluid w-100" src="https://i.pinimg.com/564x/6f/ec/0b/6fec0b0242d880c6e72155fb945ebc5b.jpg" alt="...">
                    </div>
                    <h6 class="reset-anchor">Symphony Of Birds</h6>
                    <p class="small text-muted">Rp 25.000.000</p>
                </div>
            </div>
            <!-- PRODUCT 6-->
            <div class="col-xl-3 col-lg-4 col-sm-6">
                <div class="text-center">
                    <div class="position-relative mb-3">
                        <img class="img-fluid w-100" src="https://i.pinimg.com/564x/61/ce/21/61ce210713523bd1927713e064c9e8e3.jpg" alt="...">
                    </div>
                    <h6 class="reset-anchor">Model's Expression</h6>
                    <p class="small text-muted">Rp 12.500.000</p>
                </div>
            </div>
            <!-- PRODUCT 7-->
            <div class="col-xl-3 col-lg-4 col-sm-6">
                <div class="text-center">
                    <div class="position-relative mb-3">
                        <img class="img-fluid w-100" src="https://i.pinimg.com/236x/00/07/9c/00079c27007748d2b491fc8d125fdb25.jpg" alt="...">
                    </div>
                    <h6 class="reset-anchor">Japan Bamboo's and The Sun</h6>
                    <p class="small text-muted">Rp 7.500.000</p>
                </div>
            </div>
            <!-- PRODUCT 8-->
            <div class="col-xl-3 col-lg-4 col-sm-6">
                <div class="text-center">
                    <div class="position-relative mb-3">
                        <img class="img-fluid w-100" src="https://i.pinimg.com/564x/86/05/46/8605467c54364759429043782ea97c62.jpg" alt="...">
                    </div>
                    <h6 class="reset-anchor">The Faded Flower</h6>
                    <p class="small text-muted">Rp 18.500.000</p>
                </div>
            </div>
        </div>
    </section>
    <!-- SERVICES-->
    <section class="py-5 bg-light mb-3">
        <div class="container">
            <div class="row text-center">
                <div class="col-lg-4 mb-3 mb-lg-0">
                    <div class="d-inline-block">
                        <div class="media align-items-end">
                            <svg class="svg-icon svg-icon-big svg-icon-light">
                                <use xlink:href="#delivery-time-1"> </use>
                            </svg>
                            <div class="media-body text-left ml-3">
                                <h6 class="text-uppercase mb-1">Bebas Biaya Kirim</h6>
                                <p class="text-small mb-0 text-muted">Pengiriman ke seluruh Indonesia</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 mb-3 mb-lg-0">
                    <div class="d-inline-block">
                        <div class="media align-items-end">
                            <svg class="svg-icon svg-icon-big svg-icon-light">
                                <use xlink:href="#helpline-24h-1"> </use>
                            </svg>
                            <div class="media-body text-left ml-3">
                                <h6 class="text-uppercase mb-1">Layanan 24 x 7</h6>
                                <p class="text-small mb-0 text-muted">Anda mendapatkan layanan terbaik kami</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="d-inline-block">
                        <div class="media align-items-end">
                            <svg class="svg-icon svg-icon-big svg-icon-light">
                                <use xlink:href="#label-tag-1"> </use>
                            </svg>
                            <div class="media-body text-left ml-3">
                                <h6 class="text-uppercase mb-1">Lebih Banyak Diskon</h6>
                                <p class="text-small mb-0 text-muted">Berbagai macam produk kami yang diskon</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>

<script>
    function injectSvgSprite(path) {

        var ajax = new XMLHttpRequest();
        ajax.open("GET", path, true);
        ajax.send();
        ajax.onload = function(e) {
            var div = document.createElement("div");
            div.className = 'd-none';
            div.innerHTML = ajax.responseText;
            document.body.insertBefore(div, document.body.childNodes[0]);
        }
    }
    injectSvgSprite('https://bootstraptemple.com/files/icons/orion-svg-sprite.svg');
</script>