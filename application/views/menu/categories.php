<div class="container">
    <!-- HERO SECTION-->
    <section class="py-5 bg-light">
        <div class="container">
            <div class="row px-4 px-lg-5 py-lg-4 align-items-center">
                <div class="col-lg-6">
                    <h1 class="h2 text-uppercase mb-0">Kategori Produk</h1>
                </div>
                <div class="col-lg-6 text-lg-right">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb justify-content-lg-end mb-0 px-0">
                            <li class="breadcrumb-item"><a href="<?= base_url('menu/home'); ?>">Home</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Kategori</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </section>
    <section class="py-5">
        <div class="container p-0">
            <div class="row">
                <!-- SHOP SIDEBAR-->
                <div class="col-lg-3 order-1 order-lg-1 mb-5">
                    <h5 class="text-uppercase mb-4">Kategori</h5>
                    <div class="py-2 px-4 bg-dark text-white mb-3"><strong class="small text-uppercase font-weight-bold">Aliran Lukisan</strong></div>
                    <ul class="list-unstyled small text-muted pl-lg-4 font-weight-normal">
                        <?php foreach ($genre as $data) : ?>
                            <li class="mb-2"><a class="reset-anchor" href="<?= base_url('categories/genre?filter=') . $data['nama_aliran']; ?>"><?= $data['nama_aliran']; ?></a></li>
                        <?php endforeach; ?>
                    </ul>
                    <div class="py-2 px-4 bg-light mb-3"><strong class="small text-uppercase font-weight-bold">Objek Lukisan</strong></div>
                    <ul class="list-unstyled small text-muted pl-lg-4 font-weight-normal">
                        <?php foreach ($object as $data) : ?>
                            <li class="mb-2"><a class="reset-anchor" href="<?= base_url('categories/object?filter=') . $data['nama_objek']; ?>"><?= $data['nama_objek']; ?></a></li>
                        <?php endforeach; ?>
                    </ul>
                    <h6 class="text-uppercase mb-3">Format Tampilan</h6>
                    <div class="custom-control custom-radio">
                        <input class="custom-control-input" id="customRadio1" type="radio" name="customRadio" checked>
                        <label class="custom-control-label text-small" for="customRadio1">Tampilkan Semua</label>
                    </div>
                    <div class="custom-control custom-radio">
                        <input class="custom-control-input" id="customRadio2" type="radio" name="customRadio" disabled>
                        <label class="custom-control-label text-small" for="customRadio2">Penawaran Terbaik</label>
                    </div>
                    <div class="custom-control custom-radio">
                        <input class="custom-control-input" id="customRadio3" type="radio" name="customRadio" disabled>
                        <label class="custom-control-label text-small" for="customRadio3">Lelang</label>
                    </div>
                    <div class="custom-control custom-radio">
                        <input class="custom-control-input" id="customRadio4" type="radio" name="customRadio" disabled>
                        <label class="custom-control-label text-small" for="customRadio4">Habis Terjual</label>
                    </div>
                </div>
                <!-- SHOP LISTING-->
                <div class="col-lg-9 order-2 order-lg-2 mb-lg-0">
                    <div class="row mb-3 align-items-center">
                        <div class="col-lg-6 mb-2 mb-lg-0">
                            <p class="text-small text-muted mb-0">Showing 1–12 of <?= $this->ModelProduct->getProduk()->num_rows(); ?> results</p>
                        </div>
                    </div>
                    <div class="row">
                        <!-- PRODUCT-->
                        <?php foreach ($produk as $produk) : ?>
                            <div class="col-lg-4 col-sm-6">
                                <div class="product text-center">
                                    <div class="mb-3 position-relative">
                                        <a class="d-block" href=""><img class="img-fluid w-100" src="<?= base_url('assets/'); ?>img/<?= $produk['image_produk']; ?>" alt="..."></a>
                                        <div class="product-overlay">
                                            <ul class="mb-0 list-inline">
                                                <li class="list-inline-item m-0 p-0"><a class='btn btn-sm btn-dark' href="<?= base_url('menu/tambahproduk/' . $produk['id_produk']); ?>">Tambahkan ke Troli</a></li>
                                                <li class="list-inline-item mr-0"><a class="btn btn-sm btn-outline-dark" href="<?= base_url('product/detail/' . $produk['id_produk']); ?>"><i class="fas fa-expand"></i></a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <h6> <a class="reset-anchor" href=""><?= $produk['nama_produk']; ?></a></h6>
                                    <p class="small text-muted"><?= "Rp " . number_format($produk['harga'], 0, '', '.'); ?></p>
                                </div>
                            </div>
                        <?php endforeach; ?>
                    </div>
                    <!-- PAGINATION-->
                    <nav aria-label="Page navigation example">
                        <ul class="pagination justify-content-center justify-content-lg-end">
                            <li class="page-item"><a class="page-link" href="#" aria-label="Previous"><span aria-hidden="true">«</span></a></li>
                            <li class="page-item active"><a class="page-link" href="#">1</a></li>
                            <li class="page-item"><a class="page-link" href="#" aria-label="Next"><span aria-hidden="true">»</span></a></li>
                        </ul>
                    </nav>
                </div>
            </div>
        </div>
    </section>
</div>

<!-- Nouislider Config-->
<script>
    var range = document.getElementById('range');
    noUiSlider.create(range, {
        range: {
            'min': 0,
            'max': 2000
        },
        step: 5,
        start: [100, 1000],
        margin: 300,
        connect: true,
        direction: 'ltr',
        orientation: 'horizontal',
        behaviour: 'tap-drag',
        tooltips: true,
        format: {
            to: function(value) {
                return '$' + value;
            },
            from: function(value) {
                return value.replace('', '');
            }
        }
    });
</script>
<script>
    // ------------------------------------------------------- //
    //   Inject SVG Sprite - 
    //   see more here 
    //   https://css-tricks.com/ajaxing-svg-sprite/
    // ------------------------------------------------------ //
    function injectSvgSprite(path) {

        var ajax = new XMLHttpRequest();
        ajax.open("GET", path, true);
        ajax.send();
        ajax.onload = function(e) {
            var div = document.createElement("div");
            div.className = 'd-none';
            div.innerHTML = ajax.responseText;
            document.body.insertBefore(div, document.body.childNodes[0]);
        }
    }
    // this is set to BootstrapTemple website as you cannot 
    // inject local SVG sprite (using only 'icons/orion-svg-sprite.svg' path)
    // while using file:// protocol
    // pls don't forget to change to your domain :)
    injectSvgSprite('https://bootstraptemple.com/files/icons/orion-svg-sprite.svg');
</script>