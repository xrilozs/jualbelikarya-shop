<!-- HERO SECTION-->
<div class="container">
    <section class="hero pb-3 bg-cover bg-center d-flex align-items-center" style="background-image: url(https://farm3.staticflickr.com/2783/4120420946_4e1a8473a5_o.jpg);">
        <div class="container py-5">
            <div class="row px-4 px-lg-5">
                <div class="col-lg-6">
                    <p class="small text-uppercase mb-2" style="color: ghostwhite;">Spesial Tahun Baru 2023</p>
                    <h1 class="h2 text-uppercase mb-3">Diskon 20% Khusus Hari Ini</h1><a class="btn btn-dark" id="explore" href="<?= base_url('menu/categories'); ?>">Cari Koleksi Lukisan</a>
                </div>
            </div>
        </div>
    </section>
    <!-- TRENDING PRODUCTS-->
    <section class="py-5">
        <header>
            <p class="small text-muted small text-uppercase mb-1">Lukisan Terbaik</p>
            <h2 class="h5 text-uppercase mb-4" id="highlight">Produk Terpopuler Hari Ini</h2>
        </header>
        <div class="row">
            <!-- PRODUCT-->
            <div class="col-xl-3 col-lg-4 col-sm-6">
                <div class="product text-center">
                    <div class="position-relative mb-3">
                        <div class="badge text-white badge"></div><a class="d-block" href=""><img class="img-fluid w-100" src="https://i.pinimg.com/236x/58/31/07/583107b586f3b411edfdf64a82b3c81f.jpg" alt="..."></a>
                        <div class="product-overlay">
                            <ul class="mb-0 list-inline">
                                <li class="list-inline-item m-0 p-0"><a class="btn btn-sm btn-dark" href="cart.html">Tambahkan ke Troli</a></li>
                                <li class="list-inline-item mr-0"><a class="btn btn-sm btn-outline-dark" href="#productView" data-toggle="modal"><i class="fas fa-expand"></i></a></li>
                            </ul>
                        </div>
                    </div>
                    <h6> <a class="reset-anchor" href="">Beauty Of Bloom Flower</a></h6>
                    <p class="small text-muted">Rp 10.000.000</p>
                </div>
            </div>
            <!-- PRODUCT-->
            <div class="col-xl-3 col-lg-4 col-sm-6">
                <div class="product text-center">
                    <div class="position-relative mb-3">
                        <div class="badge text-white badge-primary">Diskon</div><a class="d-block" href=""><img class="img-fluid w-100" src="https://i.pinimg.com/236x/23/8c/c1/238cc159c24f0ef83db2165241509559.jpg" alt="..."></a>
                        <div class="product-overlay">
                            <ul class="mb-0 list-inline">
                                <li class="list-inline-item m-0 p-0"><a class="btn btn-sm btn-dark" href="cart.html">Tambahkan ke Troli</a></li>
                                <li class="list-inline-item mr-0"><a class="btn btn-sm btn-outline-dark" href="#productView" data-toggle="modal"><i class="fas fa-expand"></i></a></li>
                            </ul>
                        </div>
                    </div>
                    <h6> <a class="reset-anchor" href="">Vintage Garden's</a></h6>
                    <p class="small text-muted"><s style="color: peru;"> Rp 12.500.000 </s> &nbsp; Rp 10.000.000</p>
                </div>
            </div>
            <!-- PRODUCT-->
            <div class="col-xl-3 col-lg-4 col-sm-6">
                <div class="product text-center">
                    <div class="position-relative mb-3">
                        <div class="badge text-white"></div><a class="d-block" href=""><img class="img-fluid w-100" src="https://i.pinimg.com/236x/b2/8d/6a/b28d6a0ce84bcf4386999159a1a19193.jpg" alt="..."></a>
                        <div class="product-overlay">
                            <ul class="mb-0 list-inline">
                                <li class="list-inline-item m-0 p-0"><a class="btn btn-sm btn-dark" href="cart.html">Tambahkan ke Troli</a></li>
                                <li class="list-inline-item mr-0"><a class="btn btn-sm btn-outline-dark" href="#productView" data-toggle="modal"><i class="fas fa-expand"></i></a></li>
                            </ul>
                        </div>
                    </div>
                    <h6> <a class="reset-anchor" href="">Inner Beauty Creature's</a></h6>
                    <p class="small text-muted">Rp 15.000.000</p>
                </div>
            </div>
            <!-- PRODUCT-->
            <div class="col-xl-3 col-lg-4 col-sm-6">
                <div class="product text-center">
                    <div class="position-relative mb-3">
                        <div class="badge text-white badge-info">Baru</div><a class="d-block" href=""><img class="img-fluid w-100" src="https://i.pinimg.com/236x/4a/19/93/4a19930fd149502d5bb296e3794317b7.jpg" alt="..."></a>
                        <div class="product-overlay">
                            <ul class="mb-0 list-inline">
                                <li class="list-inline-item m-0 p-0"><a class="btn btn-sm btn-dark" href="cart.html">Tambahkan ke Troli</a></li>
                                <li class="list-inline-item mr-0"><a class="btn btn-sm btn-outline-dark" href="#productView" data-toggle="modal"><i class="fas fa-expand"></i></a></li>
                            </ul>
                        </div>
                    </div>
                    <h6> <a class="reset-anchor" href="">Hexagon Of Destiny</a></h6>
                    <p class="small text-muted">Rp 5.000.000</p>
                </div>
            </div>
            <!-- PRODUCT-->
            <div class="col-xl-3 col-lg-4 col-sm-6">
                <div class="product text-center">
                    <div class="position-relative mb-3">
                        <div class="badge text-white badge-danger">Terjual</div><a class="d-block" href=""><img class="img-fluid w-100" src="https://i.pinimg.com/564x/6f/ec/0b/6fec0b0242d880c6e72155fb945ebc5b.jpg" alt="..."></a>
                        <div class="product-overlay">
                            <ul class="mb-0 list-inline">
                                <li class="list-inline-item m-0 p-0"><a class="btn btn-sm btn-dark" href="cart.html">Tambahkan ke Troli</a></li>
                                <li class="list-inline-item mr-0"><a class="btn btn-sm btn-outline-dark" href="#productView" data-toggle="modal"><i class="fas fa-expand"></i></a></li>
                            </ul>
                        </div>
                    </div>
                    <h6> <a class="reset-anchor" href="">Symphony Of Birds</a></h6>
                    <p class="small text-muted">Rp 25.000.000</p>
                </div>
            </div>
            <!-- PRODUCT-->
            <div class="col-xl-3 col-lg-4 col-sm-6">
                <div class="product text-center">
                    <div class="position-relative mb-3">
                        <div class="badge text-white"></div><a class="d-block" href=""><img class="img-fluid w-100" src="https://i.pinimg.com/564x/61/ce/21/61ce210713523bd1927713e064c9e8e3.jpg" alt="..."></a>
                        <div class="product-overlay">
                            <ul class="mb-0 list-inline">
                                <li class="list-inline-item m-0 p-0"><a class="btn btn-sm btn-dark" href="cart.html">Tambahkan ke Troli</a></li>
                                <li class="list-inline-item mr-0"><a class="btn btn-sm btn-outline-dark" href="#productView" data-toggle="modal"><i class="fas fa-expand"></i></a></li>
                            </ul>
                        </div>
                    </div>
                    <h6> <a class="reset-anchor" href="">Model's Expression</a></h6>
                    <p class="small text-muted">Rp 12.500.000</p>
                </div>
            </div>
            <!-- PRODUCT-->
            <div class="col-xl-3 col-lg-4 col-sm-6">
                <div class="product text-center">
                    <div class="position-relative mb-3">
                        <div class="badge text-white badge-"></div><a class="d-block" href=""><img class="img-fluid w-100" src="https://i.pinimg.com/236x/00/07/9c/00079c27007748d2b491fc8d125fdb25.jpg" alt="..."></a>
                        <div class="product-overlay">
                            <ul class="mb-0 list-inline">
                                <li class="list-inline-item m-0 p-0"><a class="btn btn-sm btn-dark" href="cart.html">Tambahkan ke Troli</a></li>
                                <li class="list-inline-item mr-0"><a class="btn btn-sm btn-outline-dark" href="#productView" data-toggle="modal"><i class="fas fa-expand"></i></a></li>
                            </ul>
                        </div>
                    </div>
                    <h6> <a class="reset-anchor" href="">Japan Bamboo's and The Sun</a></h6>
                    <p class="small text-muted">Rp 7.500.000</p>
                </div>
            </div>
            <!-- PRODUCT-->
            <div class="col-xl-3 col-lg-4 col-sm-6">
                <div class="product text-center">
                    <div class="position-relative mb-3">
                        <div class="badge text-white badge-"></div><a class="d-block" href=""><img class="img-fluid w-100" src="https://i.pinimg.com/564x/86/05/46/8605467c54364759429043782ea97c62.jpg" alt="..."></a>
                        <div class="product-overlay">
                            <ul class="mb-0 list-inline">
                                <li class="list-inline-item m-0 p-0"><a class="btn btn-sm btn-dark" href="cart.html">Tambahkan ke Troli</a></li>
                                <li class="list-inline-item mr-0"><a class="btn btn-sm btn-outline-dark" href="#productView" data-toggle="modal"><i class="fas fa-expand"></i></a></li>
                            </ul>
                        </div>
                    </div>
                    <h6> <a class="reset-anchor" href="">The Faded Flower</a></h6>
                    <p class="small text-muted">Rp 18.500.000</p>
                </div>
            </div>
        </div>
    </section>
    <!-- SERVICES-->
    <section class="py-5 bg-light mb-3">
        <div class="container">
            <div class="row text-center">
                <div class="col-lg-4 mb-3 mb-lg-0">
                    <div class="d-inline-block">
                        <div class="media align-items-end">
                            <svg class="svg-icon svg-icon-big svg-icon-light">
                                <use xlink:href="#delivery-time-1"> </use>
                            </svg>
                            <div class="media-body text-left ml-3">
                                <h6 class="text-uppercase mb-1">Bebas Biaya Kirim</h6>
                                <p class="text-small mb-0 text-muted">Pengiriman ke seluruh Indonesia</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 mb-3 mb-lg-0">
                    <div class="d-inline-block">
                        <div class="media align-items-end">
                            <svg class="svg-icon svg-icon-big svg-icon-light">
                                <use xlink:href="#helpline-24h-1"> </use>
                            </svg>
                            <div class="media-body text-left ml-3">
                                <h6 class="text-uppercase mb-1">Layanan 24 x 7</h6>
                                <p class="text-small mb-0 text-muted">Anda mendapatkan layanan terbaik kami</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="d-inline-block">
                        <div class="media align-items-end">
                            <svg class="svg-icon svg-icon-big svg-icon-light">
                                <use xlink:href="#label-tag-1"> </use>
                            </svg>
                            <div class="media-body text-left ml-3">
                                <h6 class="text-uppercase mb-1">Lebih Banyak Diskon</h6>
                                <p class="text-small mb-0 text-muted">Berbagai macam produk kami yang diskon</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>

<script>
    function injectSvgSprite(path) {

        var ajax = new XMLHttpRequest();
        ajax.open("GET", path, true);
        ajax.send();
        ajax.onload = function(e) {
            var div = document.createElement("div");
            div.className = 'd-none';
            div.innerHTML = ajax.responseText;
            document.body.insertBefore(div, document.body.childNodes[0]);
        }
    }
    injectSvgSprite('https://bootstraptemple.com/files/icons/orion-svg-sprite.svg');
</script>