<div class="container">
    <!-- HERO SECTION-->
    <section class="py-5 bg-light">
        <div class="container">
            <div class="row px-4 px-lg-5 py-lg-4 align-items-center">
                <div class="col-lg-6">
                    <h1 class="h2 text-uppercase mb-0">Check-out Belanja</h1>
                </div>
                <div class="col-lg-6 text-lg-right">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb justify-content-lg-end mb-0 px-0">
                            <li class="breadcrumb-item"><a href="<?= base_url('menu/home'); ?>">Home</a></li>
                            <li class="breadcrumb-item"><a href="<?= base_url('menu/cart'); ?>">Troli</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Checkout</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </section>
    <section class="py-3">
        <!-- BILLING ADDRESS-->
        <h2 class="h5 text-uppercase mb-4">Pesanan</h2>
        <form class="validate-form" action="<?= base_url('checkout/order'); ?>" method="post" enctype="multipart/form-data">
        <?php
            $id_order = "";
            $transaction = "failed";
            $pembayaran_btn = 'danger';
            if($order){
                $id_order = $order['id_order'];
                $transaction = $order['transaction'] ? $order['transaction'] : 'failed';
                $pembayaran_btn = $transaction == 'success' ? 'success' : 'danger'; 
            }
        ?>
            <input type="hidden" name="id_order" id="id_order" value="<?=$id_order;?>">
            <input type="hidden" name="transaction" id="transaction" value="<?=$transaction;?>">
            <div class="row">
                <div class="col-lg-8">
                    <div class="row">
                        <div class="col-lg-12 form-group" style="text-align: center;">
                            <label class="text-small text-uppercase" for="NamaLengkap">Nama Lengkap</label>
                            <input class="form-control form-control-lg" name="NamaLengkap" id="NamaLengkap" type="text" placeholder="Masukkan Nama Lengkap" style="text-align: center;" required>
                        </div>

                        <div class="col-lg-6 form-group">
                            <label class="text-small text-uppercase" for="email">Email</label>
                            <input class="form-control form-control-lg" name="email" id="email" type="email" placeholder="<?= $user['email']; ?>" disabled>
                        </div>
                        <?= form_error('email', '<small class="text-danger pl-3">', '</small>'); ?>

                        <div class="col-lg-6 form-group">
                            <label class="text-small text-uppercase" for="nohp">Nomor Handphone</label>
                            <input class="form-control form-control-lg" name="nohp" id="nohp" type="tel" placeholder="cont : +62 123456789" required>
                        </div>
                        <?= form_error('nohp', '<small class="text-danger pl-3">', '</small>'); ?>

                        <div class="col-lg-6 form-group">
                            <label class="text-small text-uppercase validate-input" for="negara">Negara</label>
                            <select class="selectpicker country" name="negara" id="negara" data-width="fit" data-style="form-control form-control-lg" data-title="Pilih Negara Asal" required>
                                <option value="Afghanistan">Afghanistan</option>
                                <option value="Albania">Albania</option>
                                <option value="Algeria">Algeria</option>
                                <option value="AS">American Samoa</option>
                                <option value="Andorra">Andorra</option>
                                <option value="Angola">Angola</option>
                                <option value="AI">Anguilla</option>
                                <option value="AG">Antigua and Barbuda</option>
                                <option value="Argentina">Argentina</option>
                                <option value="Armenia">Armenia</option>
                                <option value="AW">Aruba</option>
                                <option value="Australia">Australia</option>
                                <option value="Austria">Austria</option>
                                <option value="Azerbaijan">Azerbaijan</option>
                                <option value="BS">Bahamas</option>
                                <option value="Bahrain">Bahrain</option>
                                <option value="Bangladesh">Bangladesh</option>
                                <option value="BB">Barbados</option>
                                <option value="Belarus">Belarus</option>
                                <option value="Belgium">Belgium</option>
                                <option value="BZ">Belize</option>
                                <option value="BJ">Benin</option>
                                <option value="BM">Bermuda</option>
                                <option value="Bhutan">Bhutan</option>
                                <option value="Bolivia">Bolivia</option>
                                <option value="Bosnia and Herzegovina">Bosnia and Herzegovina</option>
                                <option value="Botswana">Botswana</option>
                                <option value="BV">Bouvet Island</option>
                                <option value="Brazil">Brazil</option>
                                <option value="Brunei">Brunei</option>
                                <option value="Bulgaria">Bulgaria</option>
                                <option value="BI">Burundi</option>
                                <option value="Cambodia">Cambodia</option>
                                <option value="Cameroon">Cameroon</option>
                                <option value="Canada">Canada</option>
                                <option value="CV">Cape Verde</option>
                                <option value="KY">Cayman Islands</option>
                                <option value="Central African Republic">Central African Republic</option>
                                <option value="Chile">Chile</option>
                                <option value="China">China</option>
                                <option value="Colombia">Colombia</option>
                                <option value="KM">Comoros</option>
                                <option value="CG">Congo - Brazzaville</option>
                                <option value="CD">Congo - Kinshasa</option>
                                <option value="Costa Rica">Costa Rica</option>
                                <option value="CI">Côte d’Ivoire</option>
                                <option value="Croatia">Croatia</option>
                                <option value="Cuba">Cuba</option>
                                <option value="Cyprus">Cyprus</option>
                                <option value="Republic Czech">Czech Republic</option>
                                <option value="Denmark">Denmark</option>
                                <option value="Dominican Republic">Dominican Republic</option>
                                <option value="Ecuador">Ecuador</option>
                                <option value="Egypt">Egypt</option>
                                <option value="El Salvador">El Salvador</option>
                                <option value="GQ">Equatorial Guinea</option>
                                <option value="ER">Eritrea</option>
                                <option value="Estonia">Estonia</option>
                                <option value="Ethiopia">Ethiopia</option>
                                <option value="FJ">Fiji</option>
                                <option value="Finland">Finland</option>
                                <option value="France">France</option>
                                <option value="GA">Gabon</option>
                                <option value="GM">Gambia</option>
                                <option value="Georgia">Georgia</option>
                                <option value="Germany">Germany</option>
                                <option value="Ghana">Ghana</option>
                                <option value="GI">Gibraltar</option>
                                <option value="Greece">Greece</option>
                                <option value="Greenland">Greenland</option>
                                <option value="GD">Grenada</option>
                                <option value="GP">Guadeloupe</option>
                                <option value="GU">Guam</option>
                                <option value="Guatemala">Guatemala</option>
                                <option value="GG">Guernsey</option>
                                <option value="GN">Guinea</option>
                                <option value="GW">Guinea-Bissau</option>
                                <option value="Guyana">Guyana</option>
                                <option value="Haiti">Haiti</option>
                                <option value="Honduras">Honduras</option>
                                <option value="Hongkong">Hong Kong</option>
                                <option value="Hungary">Hungary</option>
                                <option value="Iceland">Iceland</option>
                                <option value="India">India</option>
                                <option value="Indonesia">Indonesia</option>
                                <option value="Iran">Iran</option>
                                <option value="Iraq">Iraq</option>
                                <option value="Ireland">Ireland</option>
                                <option value="IL">Israel</option>
                                <option value="Italy">Italy</option>
                                <option value="Jamaica">Jamaica</option>
                                <option value="Japan">Japan</option>
                                <option value="JE">Jersey</option>
                                <option value="Jordan">Jordan</option>
                                <option value="Kazakhstan">Kazakhstan</option>
                                <option value="Kenya">Kenya</option>
                                <option value="KI">Kiribati</option>
                                <option value="Kuwait">Kuwait</option>
                                <option value="Kyrgyzstan">Kyrgyzstan</option>
                                <option value="Laos">Laos</option>
                                <option value="Latvia">Latvia</option>
                                <option value="Lebanon">Lebanon</option>
                                <option value="LS">Lesotho</option>
                                <option value="Liberia">Liberia</option>
                                <option value="Libya">Libya</option>
                                <option value="LI">Liechtenstein</option>
                                <option value="Lithuania">Lithuania</option>
                                <option value="LU">Luxembourg</option>
                                <option value="Macau">Macau</option>
                                <option value="MK">Macedonia</option>
                                <option value="Madagascar">Madagascar</option>
                                <option value="Malawi">Malawi</option>
                                <option value="Malaysia">Malaysia</option>
                                <option value="Maldives">Maldives</option>
                                <option value="ML">Mali</option>
                                <option value="MT">Malta</option>
                                <option value="MQ">Martinique</option>
                                <option value="Mauritania">Mauritania</option>
                                <option value="MU">Mauritius</option>
                                <option value="YT">Mayotte</option>
                                <option value="Mexico">Mexico</option>
                                <option value="Moldova">Moldova</option>
                                <option value="Monaco">Monaco</option>
                                <option value="Mongolia">Mongolia</option>
                                <option value="Montenegro">Montenegro</option>
                                <option value="MS">Montserrat</option>
                                <option value="Morocco">Morocco</option>
                                <option value="Mozambique">Mozambique</option>
                                <option value="Myanmar">Myanmar</option>
                                <option value="Namibia">Namibia</option>
                                <option value="NR">Nauru</option>
                                <option value="Nepal">Nepal</option>
                                <option value="Netherlands">Netherlands</option>
                                <option value="NC">New Caledonia</option>
                                <option value="New Zealand">New Zealand</option>
                                <option value="Nicaragua">Nicaragua</option>
                                <option value="Nigeria">Nigeria</option>
                                <option value="North Korea">North Korea</option>
                                <option value="Norway">Norway</option>
                                <option value="Oman">Oman</option>
                                <option value="Pakistan">Pakistan</option>
                                <option value="PW">Palau</option>
                                <option value="Palestine">Palestine</option>
                                <option value="Panama">Panama</option>
                                <option value="Papua New Guinea">Papua New Guinea</option>
                                <option value="Paraguay">Paraguay</option>
                                <option value="Peru">Peru</option>
                                <option value="Philippines">Philippines</option>
                                <option value="Poland">Poland</option>
                                <option value="Portugal">Portugal</option>
                                <option value="Puerto Rico">Puerto Rico</option>
                                <option value="Qatar">Qatar</option>
                                <option value="Romania">Romania</option>
                                <option value="Russia">Russia</option>
                                <option value="Rwanda">Rwanda</option>
                                <option value="WS">Samoa</option>
                                <option value="San Marino">San Marino</option>
                                <option value="ST">São Tomé and Príncipe</option>
                                <option value="Saudi Arabia">Saudi Arabia</option>
                                <option value="Senegal">Senegal</option>
                                <option value="Serbia">Serbia</option>
                                <option value="Sierra Leone">Sierra Leone</option>
                                <option value="Singapore">Singapore</option>
                                <option value="Slovakia">Slovakia</option>
                                <option value="Slovenia">Slovenia</option>
                                <option value="SB">Solomon Islands</option>
                                <option value="Somalia">Somalia</option>
                                <option value="South Africa">South Africa</option>
                                <option value="GS">South Georgia</option>
                                <option value="South Korea">South Korea</option>
                                <option value="Spain">Spain</option>
                                <option value="Sri Lanka">Sri Lanka</option>
                                <option value="Sudan">Sudan</option>
                                <option value="Suriname">Suriname</option>
                                <option value="Swaziland">Swaziland</option>
                                <option value="Sweden">Sweden</option>
                                <option value="Switzerland">Switzerland</option>
                                <option value="Syria">Syria</option>
                                <option value="Taiwan">Taiwan</option>
                                <option value="Tajikistan">Tajikistan</option>
                                <option value="Tanzania">Tanzania</option>
                                <option value="Thailand">Thailand</option>
                                <option value="TL">Timor-Leste</option>
                                <option value="Togo">Togo</option>
                                <option value="TK">Tokelau</option>
                                <option value="Tonga">Tonga</option>
                                <option value="TT">Trinidad and Tobago</option>
                                <option value="Tunisia">Tunisia</option>
                                <option value="Turkey">Turkey</option>
                                <option value="Turkmenistan">Turkmenistan</option>
                                <option value="TV">Tuvalu</option>
                                <option value="Uganda">Uganda</option>
                                <option value="Ukraine">Ukraine</option>
                                <option value="UAE">United Arab Emirates</option>
                                <option value="UK">United Kingdom</option>
                                <option value="USA">United State of America</option>
                                <option value="Uruguay">Uruguay</option>
                                <option value="Uzbekistan">Uzbekistan</option>
                                <option value="VU">Vanuatu</option>
                                <option value="VA">Vatican City</option>
                                <option value="Venezuela">Venezuela</option>
                                <option value="Vietnam">Vietnam</option>
                                <option value="Yemen">Yemen</option>
                                <option value="Zambia">Zambia</option>
                                <option value="Zimbabwe">Zimbabwe</option>
                            </select>
                        </div>
                        <?= form_error('negara', '<small class="text-danger pl-3">', '</small>'); ?>

                        <div class="col-lg-6 form-group">
                            <label class="text-small text-uppercase" for="kota">Kota</label>
                            <input class="form-control form-control-lg" name="kota" id="kota" type="text" placeholder="Kota Asal Anda" required>
                        </div>
                        <?= form_error('kota', '<small class="text-danger pl-3">', '</small>'); ?>

                        <div class="col-lg-12 form-group">
                            <label class="text-small text-uppercase" for="alamat">Alamat Lengkap</label>
                            <input class="form-control form-control-lg" name="alamat" id="alamat" type="text" placeholder="Tuliskan Alamat Lengkap Pengiriman" required>
                        </div>
                        <?= form_error('alamat', '<small class="text-danger pl-3">', '</small>'); ?>

                    </div>
                </div>
                <!-- ORDER SUMMARY-->
                <div class="col-lg-4" id="orderlist">
                    <div class="card border-0 rounded-0 p-lg-4 bg-light mb-3">
                        <div class="card-body">
                            <h5 class="text-uppercase mb-4">Pesananmu</h5>
                            <ul class="list-unstyled mb-0">
                                <?php 
                                    $total = 0; 
                                    $subtotal = 0;
                                foreach ($cart as $troli) { ?>
                                    <li class="d-flex align-items-center justify-content-between">
                                        <div class="row">
                                            <div class="col">
                                                <strong class="small font-weight-bold">
                                                    <?= $troli['nama_produk']; ?>
                                                </strong>
                                            </div>
                                            <div class="col">
                                                <span class="text-muted small">
                                                    <?= "Rp " . number_format($troli['harga']*$troli['jml_beli'], 0, '', '.'); ?>
                                                </span>
                                            </div>
                                        </div>
                                    </li>
                                <?php
                                    $subtotal += $troli['harga']*$troli['jml_beli'];
                                    $ongkir = 0;
                                    $diskon = 20 / 100;
                                    $ppn = 10 / 100;
                                    $total = (($ppn * $subtotal) + $subtotal + $ongkir) - ($subtotal * $diskon);
                                } ?>
                                <li class="border-bottom my-2"></li>
                                <li class="">
                                    <input name="subtotal" id="subtotal" type="number" value="<?=$subtotal;?>" hidden>
                                    <div class="row">
                                        <div class="col">
                                            <span class="small">Subtotal</span>
                                        </div>
                                        <div class="col">
                                            <span class="text-muted small">
                                                <?= "Rp " . number_format($subtotal, 0, '', '.'); ?>
                                            </span>
                                        </div>
                                    </div>
                                </li>
                                <li class="">
                                    <input name="ongkir" id="ongkir" type="text" value="0" hidden>
                                    <div class="row">
                                        <div class="col">
                                            <span class="small">Biaya Pengiriman</span>
                                        </div>
                                        <div class="col">
                                            <span class="text-muted small">
                                                Rp 0
                                            </span>
                                        </div>
                                    </div>
                                </li>
                                <li class="">
                                    <input name="diskon" id="diskon" type="text" value="20%" hidden>
                                    <div class="row">
                                        <div class="col-lg-6">
                                            <span class="small">Diskon</span>
                                        </div>
                                        <div class="col-lg-6">
                                            <span class="text-muted small">
                                                20%
                                            </span>
                                        </div>
                                    </div>
                                </li>
                                <li class="">
                                    <input name="ppn" id="ppn" type="text" value="10%" hidden>
                                    <div class="row">
                                        <div class="col-6">
                                            <span class="small">PPN</span>
                                        </div>
                                        <div class="col-6">
                                            <span class="text-muted small">
                                                10%
                                            </span>
                                        </div>
                                    </div>
                                </li>
                                <li class="">
                                    <input name="total" id="total" type="number" value="<?= $total; ?>" hidden>
                                    <div class="row">
                                        <div class="col">
                                            <strong class="text-uppercase small font-weight-bold">Total</strong>
                                        </div>
                                        <div class="col">
                                            <span>
                                                <?= "Rp " . number_format($total, 0, '', '.'); ?>
                                            </span>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <textarea class="form-control form-control-lg" name="catatan" id="catatan" cols="38" rows="3" maxlength="150" placeholder="Tambahkan Catatan ..."></textarea>
                </div>
            </div>
            <div class="col-lg-8 mt-4 form-group" style="text-align: center;">
                <button class="btn btn-dark mb-3" type="submit">Pesan Sekarang</button>
                <button class="btn btn-<?=$pembayaran_btn;?> mb-3" id="pembayaran-btn" data-toggle="modal" data-target="#payment" type="button">Pembayaran</button>
            </div>
        </form>
    </section>
    <section>
        <!-- Payment Gateway -->
        <div id="payment" class="modal fade">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="container">
                        <div class="row">
                            <div class="mx-auto">
                                <div class="card ">
                                    <div class="card-header">
                                        <div class="bg-white shadow-sm pt-4 pl-2 pr-2 pb-2">
                                            <!-- Credit Card Form Tabs -->
                                            <ul role="tablist" class="nav bg-light nav-pills rounded nav-fill mb-3">
                                                <li class="nav-item"> <a data-toggle="pill" href="#credit-card" class="nav-link active "> <i class="fas fa-credit-card mr-2"></i> Credit Card </a> </li>
                                                <li class="nav-item"> <a data-toggle="pill" href="#net-banking" class="nav-link "> <i class="fas fa-mobile-alt mr-2"></i> Mobile Banking </a> </li>
                                            </ul>
                                        </div>
                                        <!-- End -->

                                        <!-- Credit Card Form Content -->
                                        <div class="tab-content">
                                            <!-- credit card info-->
                                            <div id="credit-card" class="tab-pane fade show active pt-3">
                                                <div id="cc-alert"></div>
                                                <form role="form" id="payment-cc-form">
                                                    <input name="subtotal" id="subtotal" type="number" value="<?=$subtotal;?>" hidden>
                                                    <input name="ongkir" id="ongkir" type="text" value="0" hidden>
                                                    <input name="diskon" id="diskon" type="text" value="20%" hidden>
                                                    <input name="ppn" id="ppn" type="text" value="10%" hidden>
                                                    <input name="total" id="total" type="number" value="<?= $total; ?>" hidden>

                                                    <div class="form-group"> 
                                                        <label for="username">
                                                            <h6>Card Owner</h6>
                                                        </label> 
                                                        <input type="text" name="username" placeholder="Card Owner Name" required class="form-control "> 
                                                    </div>
                                                    <div class="form-group"> 
                                                        <label for="cardNumber">
                                                            <h6>Card number</h6>
                                                        </label>
                                                        <div class="input-group"> 
                                                            <input type="text" name="cardNumber" placeholder="Valid card number" class="form-control " required>
                                                            <div class="input-group-append"> 
                                                                <span class="input-group-text text-muted"> 
                                                                    <i class="fab fa-cc-visa mx-1"></i> 
                                                                    <i class="fab fa-cc-mastercard mx-1"></i> 
                                                                    <i class="fab fa-cc-amex mx-1"></i> 
                                                                </span> 
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-sm-8">
                                                            <div class="form-group"> 
                                                                <label>
                                                                    <span class="hidden-xs">
                                                                        <h6>Expiration Date</h6>
                                                                    </span>
                                                                </label>
                                                                <div class="input-group"> 
                                                                    <input type="number" placeholder="MM" name="" class="form-control" required> 
                                                                    <input type="number" placeholder="YY" name="" class="form-control" required> 
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-4">
                                                            <div class="form-group mb-4"> 
                                                                <label data-toggle="tooltip" title="Three digit CV code on the back of your card">
                                                                    <h6>CVV <i class="fa fa-question-circle d-inline"></i></h6>
                                                                </label> 
                                                                <input type="text" required class="form-control"> 
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="card-footer"> 
                                                        <button type="submit" id="payment-cc-button" class="subscribe btn btn-primary btn-block shadow-sm"> 
                                                            Confirm Payment 
                                                        </button>
                                                </form>
                                            </div>
                                        </div>
                                        <!-- End -->

                                        <!-- Bank Transfer Info -->
                                        <div id="net-banking" class="tab-pane fade pt-3">
                                            <div class="form-group ">
                                                <div id="mobile-alert"></div>
                                                <form role="form" id="payment-mobile-form">
                                                <input name="subtotal" id="subtotal" type="number" value="<?=$subtotal;?>" hidden>
                                                <input name="ongkir" id="ongkir" type="text" value="0" hidden>
                                                <input name="diskon" id="diskon" type="text" value="20%" hidden>
                                                <input name="ppn" id="ppn" type="text" value="10%" hidden>
                                                <input name="total" id="total" type="number" value="<?= $total; ?>" hidden>
                                                <label for="Select Your Bank">
                                                    <h6>Select your Bank</h6>
                                                </label> 
                                                <select class="form-control" id="bank" required>
                                                    <option value="" selected disabled>
                                                        --Silahkan pilih banknya--
                                                    </option>
                                                    <option>Bank Central Asia (BCA)</option>
                                                    <option>Bank Mandiri</option>
                                                    <option>Bank Niaga</option>
                                                    <option>Bank BNI</option>
                                                    <option>Bank BRI</option>
                                                    <option>Citibank</option>
                                                </select> 
                                            </div>
                                            <div class="form-group">
                                                <p> 
                                                    <button type="submit" id="payment-mobile-btn" class="btn btn-primary">
                                                        <i class="fas fa-mobile-alt mr-2"></i> Proceed Payment
                                                    </button> 
                                                    </form>
                                                </p>
                                            </div>
                                            <p class="text-muted">Catatan: Setelah mengklik tombol, pembayaran kamu akan dikonfirmasi oleh sistem. Setelah menyelesaikan proses pembayaran, kamu akan diarahkan kembali ke website untuk melihat detail pesanan. </p>
                                        </div>
                                        <!-- End -->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<script>
    // ------------------------------------------------------- //
    //   Inject SVG Sprite - 
    //   see more here 
    //   https://css-tricks.com/ajaxing-svg-sprite/
    // ------------------------------------------------------ //
    function injectSvgSprite(path) {

        var ajax = new XMLHttpRequest();
        ajax.open("GET", path, true);
        ajax.send();
        ajax.onload = function(e) {
            var div = document.createElement("div");
            div.className = 'd-none';
            div.innerHTML = ajax.responseText;
            document.body.insertBefore(div, document.body.childNodes[0]);
        }
    }
    // this is set to BootstrapTemple website as you cannot 
    // inject local SVG sprite (using only 'icons/orion-svg-sprite.svg' path)
    // while using file:// protocol
    // pls don't forget to change to your domain :)
    injectSvgSprite('https://bootstraptemple.com/files/icons/orion-svg-sprite.svg');
</script>