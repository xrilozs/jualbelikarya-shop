<div class="container">
    <!-- HERO SECTION-->
    <section class="py-5 bg-light">
        <div class="container">
            <div class="row px-4 px-lg-5 py-lg-4 align-items-center">
                <div class="col-lg-6">
                    <h1 class="h2 text-uppercase mb-0">Troli Belanja</h1>
                </div>
                <div class="col-lg-6 text-lg-right">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb justify-content-lg-end mb-0 px-0">
                            <li class="breadcrumb-item"><a href="<?= base_url('menu/home'); ?>">Home</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Troli</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </section>
    <section class="py-5">
        <h2 class="h5 text-uppercase mb-4">Daftar Belanja</h2>
        <div class="row">
            <div class="col-lg-12 mb-4 mb-lg-0">
                <!-- CART TABLE-->
                <div class="table-responsive mb-4">
                    <table class="table">
                        <thead class="bg-light">
                            <tr>
                                <th class="border-0" scope="col"> <strong class="text-small text-uppercase">Produk</strong></th>
                                <th class="border-0" scope="col"> <strong class="text-small text-uppercase">Harga</strong></th>
                                <th class="border-0" scope="col"> <strong class="text-small text-uppercase">Banyaknya</strong></th>
                                <th class="border-0" scope="col"> <strong class="text-small text-uppercase">Subtotal</strong></th>
                                <th class="border-0" scope="col"> </th>
                            </tr>
                        </thead>
                        <?php foreach ($cart as $key=>$troli) { ?>
                            <tr>
                                <th class="pl-0 border-0" scope="row">
                                    <div class="media align-items-center"><img src="<?= base_url('assets/img/' . $troli['image_produk']); ?>" alt="..." width="70">
                                        <div class="media-body ml-3"><strong class="h6"><?= $troli['nama_produk']; ?></strong></div>
                                    </div>
                                </th>
                                <td class="align-middle border-0">
                                    <p class="mb-0 small"><?= "Rp " . number_format($troli['harga'], 0, '', '.'); ?></p>
                                </td>
                                <td class="align-middle border-0">
                                    <div class="quantity">
                                        <button class="dec-btn p-0" data-id="<?=$key;?>" data-harga="<?=$troli['harga'];?>"><i class="fas fa-caret-left"></i></button>
                                        <input class="form-control form-control-sm border-0 shadow-0 p-0 jml_beli" name="jml_beli" data-id="<?=$troli['id_cart'];?>" type="text" inputmode="numeric" value="<?= $troli['jml_beli']; ?>" min="1" onchange="changeValue(this, <?=$troli['harga'];?>)">
                                        <button class="inc-btn p-0" data-id="<?=$key;?>" data-harga="<?=$troli['harga'];?>"><i class="fas fa-caret-right"></i></button>
                                    </div>
                                </td>
                                <!-- $subtotal = $troli['harga'] * $banyaknya -->
                                <td class="align-middle border-0">
                                    <p class="mb-0 small" id="subtotal-<?=$key;?>"><?= "Rp " . number_format($troli['harga']*$troli['jml_beli'], 0, '', '.'); ?></p>
                                </td>
                                <td class="align-middle border-0">
                                    <a class="reset-anchor" id="trash" title="Hapus" href="<?= base_url('cart/delete/' . $troli['id_produk']); ?>"><i class="fas fa-trash-alt"></i></a>
                                </td>
                            </tr>
                        <?php } ?>
                    </table>
                </div>
                <?= $this->session->flashdata('pesan'); ?>
                <!-- CART NAV-->
                <div class="bg-light px-4 py-3">
                    <div class="row align-items-center text-center">
                        <div class="col-md-6 mb-3 mb-md-0 text-md-left"><a class="btn btn-link p-0 text-dark btn-sm" href="<?= base_url('menu/home'); ?>"><i class="fas fa-long-arrow-alt-left mr-2"></i>Kembali Berbelanja</a></div>
                        <div class="col-md-6 text-md-right">
                            <button class="btn btn-outline-dark btn-sm" id="checkout-btn">
                                Konfirmasi Pesanan<i class="fas fa-long-arrow-alt-right ml-2"></i>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
            <!-- ORDER TOTAL-->
            <!--
            <div class="col-lg-4">
                <div class="card border-0 rounded-0 p-lg-4 bg-light">
                    <div class="card-body">
                        <h5 class="text-uppercase mb-4">Total Belanja</h5>
                        <ul class="list-unstyled mb-0">
                            <li class="d-flex align-items-center justify-content-between">
                                <strong class="text-uppercase small font-weight-bold">Subtotal</strong>
                                <ul class="list-unstyled mb-0">
                                    <li>
                                        <span class="form-text text-muted small"></span>
                                    </li>
                                </ul>
                            </li>
                            <li class="border-bottom my-2"></li>
                            <li class="d-flex align-items-center justify-content-between mb-4">
                                <strong class="text-uppercase small font-weight-bold">Total</strong>
                                <span></span>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
                        -->
        </div>
    </section>
</div>