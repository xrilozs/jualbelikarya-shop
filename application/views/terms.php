<div class="container">
    <!-- HERO SECTION-->
    <section class="py-5 bg-light">
        <div class="container">
            <div class="row px-4 px-lg-5 py-lg-4 align-items-center">
                <div class="col-lg-6">
                    <h1 class="h2 text-uppercase">Syarat dan Ketentuan</h1>
                </div>
            </div>
        </div>
    </section>
    <section class="py-3">
        <h3 class="mb-4">Perjanjian Atas Ketentuan</h3>
        <p style="text-align: justify;">
            Ketentuan Pengguna ini merupakan perjanjian yang mengikat secara hukum yang dibuat antara Anda dengan Situs, baik secara pribadi atau atas nama entitas (“Anda” sebagai pengguna, pelanggan dan pembeli) dan [admin] (“Kami” sebagai pengelola), mengenai akses Anda ke dan penggunaan situs web [Jualbelikarya] serta segala bentuk media lainnya, saluran media, situs web, atau aplikasi seluler yang terkait, tertaut, atau terhubung dengannya (secara bersama-sama disebut “Situs”). Anda setuju bahwa dengan mengakses Situs, Anda telah membaca, memahami, dan setuju untuk terikat dengan semua Persyaratan Pengguna ini.
        </p>
        <p>
        <ul id="syarat">
            <li class="mb-3" style="text-align: justify;">Aturan 1 : Situs ini ditujukan untuk pengguna yang berusia minimal 18 tahun. Orang di bawah usia 18 tahun tidak diizinkan untuk menggunakan atau mendaftar ke Situs.</li>
            <li class="mb-3" style="text-align: justify;">Aturan 2 : [Situs ini ditujukan untuk pengguna yang berusia minimal 18 tahun.] Semua pengguna di bawah umur (umumnya berusia di bawah 18 tahun) harus memiliki izin, dan diawasi langsung oleh orang tua atau wali mereka untuk menggunakan Situs. Jika Anda masih di bawah umur, Anda harus meminta orang tua atau wali Anda membaca dan menyetujui Ketentuan Penggunaan ini sebelum Anda menggunakan Situs.</li>
        </ul>
        </p>
        <p style="text-align: justify;">Jika Anda tidak setuju dengan semua Ketentuan Pengguna ini, maka Anda secara tegas dilarang menggunakan dan bertransaksi didalam Situs. Anda harus segera menghentikan penggunaan Situs. Syarat dan ketentuan tambahan yang diposting di Situs dari waktu ke waktu dengan ini secara tegas dimasukkan di sini sebagai referensi. Kami berhak, atas kebijakan kami sendiri, untuk membuat perubahan atau modifikasi pada Ketentuan Pengguna ini kapan saja dan untuk alasan apa pun.</p>
    </section>
    <section class="py-3">
        <h3 class="mb-4">Data Pengguna</h3>
        <p style="text-align: justify;">Kami akan menjaga dan melindungi data yang Anda kirimkan ke Situs untuk tujuan mengelola pesanan, akun, dan kinerja Situs serta data yang berkaitan dengan penggunaan Situs oleh Anda. Meskipun kami melakukan pencadangan data secara rutin, Anda bertanggung jawab penuh atas semua data yang Anda kirimkan atau yang terkait dengan aktivitas apa pun yang Anda lakukan menggunakan Situs. Anda setuju bahwa kami tidak akan bertanggung jawab kepada Anda atas kehilangan atau kerusakan data tersebut.</p>
    </section>
    <section class="py-3">
        <h3 class="mb-4">Pembelian dan Pembayaran</h3>
        <p style="text-align: justify;">Kami menerima bentuk pembayaran berikut: Kartu Kredit, Internet Banking, dan Mobile Banking. Anda setuju untuk memberikan informasi pembelian dan akun terkini, secara lengkap dan akurat untuk semua pembelian yang dilakukan melalui Situs. Anda selanjutnya setuju untuk segera memperbarui informasi akun dan pembayaran, termasuk alamat email, metode pembayaran, dan tanggal kedaluwarsa kartu pembayaran, sehingga kami dapat menyelesaikan transaksi Anda dan menghubungi Anda sesuai kebutuhan.</p>
        <p style="text-align: justify;">Pajak penjualan akan ditambahkan secara otomatis ke harga pembelian sebagai kewajiban Kami dalam melakukan kegiatan jual beli pada Situs. Kami juga dapat mengubah harga barang sewaktu-waktu jika diperlukan. Semua pembayaran yang ada pada Situs harus dalam Indonesia Rupiah (Rp). Anda setuju untuk membayar semua biaya dengan harga yang berlaku untuk pembelian Anda dan biaya pengiriman apa pun yang berlaku, dan Anda memberi wewenang kepada kami untuk menagih penyedia pembayaran yang Anda pilih untuk jumlah pembelian tersebut saat melakukan pemesanan.</p>
        <p style="text-align: justify;">Kami berhak untuk memperbaiki kesalahan atau kesalahan dalam penetapan harga, bahkan jika kami telah meminta atau menerima pembayaran. Kami berhak menolak pesanan apa pun yang dilakukan di luar Situs. Kami dapat, atas kebijakan kami sendiri, membatasi atau membatalkan jumlah yang dibeli per pesanan, per orang, atau per instansi. Pembatasan ini dapat mencakup pesanan yang dilakukan oleh atau di bawah akun pelanggan yang sama, metode pembayaran yang sama, dan/atau pesanan yang menggunakan alamat penagihan atau pengiriman yang sama. Kami berhak membatasi atau melarang pesanan yang menurut penilaian kami sendiri, tampaknya dilakukan oleh dealer, pengecer, atau distributor.</p>
    </section>
</div>