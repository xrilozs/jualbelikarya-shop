<section class="py-5">
    <div class="container">
        <div class="row mb-5">
            <div class="col-lg-6">
                <img class="img-fluid" src="<?= base_url('assets/'); ?>img/<?= $image_produk; ?>">
            </div>
            <!-- PRODUCT DETAILS-->
            <div class="col-lg-6">
                <h1><?= $nama_produk; ?></h1>
                <p class="text-muted lead"><?= "Rp " . number_format($harga, 0, '', '.') ?></p>
                <p class="text-small mb-4"><?= $deskripsi; ?></p>
                <div class="row align-items-stretch mb-4">
                    <div class="col-sm-10 pr-sm-0">
                        <a class="btn btn-dark btn-sm btn-block h-100 d-flex align-items-center justify-content-center px-0" href="<?= base_url('menu/tambahproduk/' . $id_produk); ?>">Tambahkan ke Troli</a>
                    </div>
                </div>
                <ul class="list-unstyled small d-inline-block">
                    <li class="py-2 mb-1 bg-white text-muted"><strong class="text-uppercase text-dark">Aliran Lukisan:</strong>
                        <?= $aliran; ?>
                    </li>
                    <li class="py-2 mb-1 bg-white text-muted"><strong class="text-uppercase text-dark">Objek Lukisan:</strong>
                        <?= $objek; ?>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</section>