<div id="content">
    <div id="content-header">
        <div id="breadcrumb"> <a href="<?= base_url('admin/dashboard'); ?>" title="Go to Dashboard" class="tip-bottom"><i class="icon-home"></i> Dashboard</a> / <a href="<?= base_url('admin/products'); ?>" class="current">Products</a> </div>
        <?= $this->session->flashdata('pesan'); ?>
        <h1>Products</h1>
    </div>
    <div class="container-fluid">
        <div class="row-fluid">
            <div class="span12">
                <div class="widget-box">
                    <div class="widget-title"> <span class="icon"><a href="" id="tambah" data-toggle="modal" data-target="#ModalAddProduct"><i class="far fa-plus" style="color: green;"></i></a> </span>
                        <h5>Add Product</h5>
                    </div>
                    <div class="widget-content nopadding">
                        <table class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>Image</th>
                                    <th>Product Name</th>
                                    <th>Price</th>
                                    <th>Description</th>
                                    <th>Manage</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                foreach ($produk as $p) : ?>
                                    <tr>
                                        <td>
                                            <picture>
                                                <source srcset="" type="image/svg+xml">
                                                <img style="height: 420px; width: 400px;" src="<?= base_url('assets/img/') . $p['image_produk']; ?>" class="img-fluid img-thumbnail" alt="...">
                                            </picture>
                                        </td>
                                        <td><?= $p['nama_produk']; ?></td>
                                        <td><?= "Rp " . number_format($p['harga'], 0, '', '.'); ?></td>
                                        <td><?= $p['deskripsi']; ?></td>
                                        <td>
                                            <a href="<?= base_url('admin/hapusProduk/') . $p['id_produk']; ?>" onclick="return confirm('Kamu yakin akan menghapus <?= $p['nama_produk']; ?>')" class="badge badge-danger" style="background-color: crimson;"><i class="fas fa-trash"></i> Hapus</a>
                                            <a href="<?= base_url('admin/ubahProduk/') . $p['id_produk']; ?>" class="badge badge-info"><i class="far fa-edit"></i> Ubah</a>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div id="ModalAddProduct" class="modal hide">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button data-dismiss="modal" class="close" type="button">×</button>
                                <h3 class="modal-title" style="text-align: center;">Add New Product</h3>
                            </div>
                            <form action="<?= base_url('admin/tambahProduk'); ?>" method="post" enctype="multipart/form-data">
                                <div class="modal-body" style="text-align: center;">
                                    <div class="form-group">
                                        <input type="file" class="form-control form-control-user" id="image" name="image">
                                    </div>
                                    <div class="form-group">
                                        <input type="text" class="form-control form-control-user" id="productname" name="productname" placeholder="Product Name">
                                    </div>
                                    <div class="form-group">
                                        <div class="input-prepend">
                                            <span class="add-on">Rp</span>
                                            <input type="number" class="form-control form-control-user" id="price" name="price" min="100000" max="1000000000" placeholder="Price">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <textarea class="form-control form-control-user" id="desc" name="desc" rows="3" maxlength="250" placeholder="Description"></textarea>
                                    </div>
                                </div>
                                <div class="modal-footer"> <button type="submit" class="btn btn-success">Confirm</button> <button type="button" class="btn" data-dismiss="modal">Cancel</button> </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="<?= base_url('assets/'); ?>js/admin/jquery.min.js"></script>
<script src="<?= base_url('assets/'); ?>js/admin/jquery.ui.custom.js"></script>
<script src="<?= base_url('assets/'); ?>js/admin/bootstrap.min.js"></script>
<script src="<?= base_url('assets/'); ?>js/admin/jquery.uniform.js"></script>
<script src="<?= base_url('assets/'); ?>js/admin/select2.min.js"></script>
<script src="<?= base_url('assets/'); ?>js/admin/jquery.dataTables.min.js"></script>
<script src="<?= base_url('assets/'); ?>js/admin/maruti.js"></script>
<script src="<?= base_url('assets/'); ?>js/admin/maruti.tables.js"></script>