<div id="content">
    <div id="content-header">
        <div id="breadcrumb"> <a href="<?= base_url('admin/dashboard'); ?>"><i class="icon-home"></i> Dashboard</a></div>
    </div>
    <div class="container-fluid">
        <div class="quick-actions_homepage">
            <ul class="quick-actions">
                <li> <a href="<?= base_url('admin/orders'); ?>"> <i class="far fa-shopping-cart"></i> Shopping Orders</a> </li>
                <li> <a href="<?= base_url(); ?>"> <i class="fal fa-browser"></i> Official Website </a> </li>
                <li> <a href="<?= base_url('admin/members'); ?>"> <i class="fal fa-users-cog"></i> Manage Users </a> </li>
                <li> <a href="<?= base_url('admin/products'); ?>"> <i class="fal fa-boxes"></i> <i class="fal fa-clipboard-list-check"></i> Manage Products </a> </li>
                <li> <a href="<?= base_url('admin/complaints'); ?>"> <i class="fal fa-list-alt"></i> Manage Complaints </a> </li>
            </ul>
        </div>

        <div class="row-fluid" style="text-align: center;">
            <div>
                <ul class="stat-boxes">
                    <li>
                        <div class="right"> <strong><?= $this->ModelProduct->getProduk()->num_rows(); ?></strong> Products </div>
                    </li>
                    <li>
                        <div class="right"> <strong><?= $this->ModelUser->getUser()->num_rows(); ?></strong> Users </div>
                    </li>
                    <li>
                        <div class="right"> <strong>1</strong> Orders </div>
                    </li>
                </ul>
            </div>
        </div>

        <div class="row-fluid">
            <div class="widget-box">
                <div class="widget-title"><span class="icon"><i class="fal fa-chart-area"></i></span>
                    <h5>Statistik Pengunjung Website</h5>
                </div>
                <div class="widget-content">
                    <div class="row-fluid">
                        <div class="span12">
                            <canvas id="orderChart"></canvas>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <hr>
        <div class="row-fluid">
            <div class="span6">
                <div class="widget-box">
                    <div class="widget-title">
                        <h5>Pesanan Terbaru</h5>
                    </div>
                    <div class="widget-content nopadding">
                        <ul class="recent-posts">
                            <li>
                                <div class="user-thumb"> <img width="40" height="40" src="https://i.pinimg.com/236x/58/31/07/583107b586f3b411edfdf64a82b3c81f.jpg"> </div>
                                <div class="article-post"> <span class="user-info"> User: Syifa Alya Zahra / Date: 1 Juni 2023 / Time: 07:10 AM </span>
                                    <p><a href="">Harap dikemas dengan baik dan aman, terima kasih</a> </p>
                                </div>
                            </li>
                            <li>
                                <div class="user-thumb"> <img width="40" height="40" src="https://i.pinimg.com/564x/6f/ec/0b/6fec0b0242d880c6e72155fb945ebc5b.jpg"> </div>
                                <div class="article-post"> <span class="user-info"> User: Andhika Rifqi Arifin / Date: 1 Mei 2023 / Time: 10:15 PM </span>
                                    <p><a href="">Harap barang diberi bungkusan agar aman</a> </p>
                                </div>
                            </li>
                            <li>
                                <div class="user-thumb"> <img width="40" height="40" src="https://i.pinimg.com/236x/00/07/9c/00079c27007748d2b491fc8d125fdb25.jpg"> </div>
                                <div class="article-post"> <span class="user-info"> User: Bima Ajisyiwa / Date: 1 April 2023 / Time: 09:50 AM </span>
                                    <p><a href="">Harap barang sesuai yang di gambar, terima kasih</a> </p>
                                </div>
                            </li>
                            <li>
                                <a class="btn btn-warning btn-mini" href="<?= base_url('admin/orders'); ?>">Lihat Semua</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="span6">
                <div class="widget-box">
                    <div class="widget-title">
                        <h5>Pengaduan Pelanggan</h5>
                    </div>
                    <div class="widget-content nopadding">
                        <ul class="recent-posts">
                            <?php
                            $i = 0;
                            foreach ($complaint as $complaint) :
                                if ($i >= 3) {
                                    break;
                                } else { ?>
                                    <li>
                                        <div class="user-thumb"> <img width="40" height="40" src="https://static.thenounproject.com/png/2932881-200.png"> </div>
                                        <div class="article-post"> <span class="user-info"> Name: <?= $complaint['nama']; ?> / Email: <?= $complaint['email']; ?> / Phone: <?= $complaint['no_hp']; ?> </span>
                                            <p><?= $complaint['isi']; ?></p>
                                        </div>
                                    </li>
                            <?php $i++;
                                }
                            endforeach; ?>
                            <li>
                                <a class="btn btn-warning btn-mini" href="<?= base_url('admin/complaints'); ?>">Lihat Semua</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="row-fluid">
            <div class="widget-box">
                <div class="widget-title">
                    <h5>Calendar</h5>
                </div>
                <div class="widget-content nopadding updates">
                    <div class="new-update clearfix">
                        <div class="update-done"><a title="" href=""><strong>Libur Hari Raya Idul Fitri 1443 H</strong></a> <span>Selamat hari raya idul fitri. Minal aidzin wal faidzin, mohon maaf lahir dan bathin.</span> </div>
                        <div class="update-date"><span class="update-day">3</span>May</div>
                    </div>
                    <div class="new-update clearfix">
                        <div class="update-done"><a title="" href=""><strong>Libur Hari Raya Idul Adha 1443 H</strong></a> <span>Selamat hari raya idul adha. Berkurban lah jika memiliki rezeki lebih.</span> </div>
                        <div class="update-date"><span class="update-day">9</span>July</div>
                    </div>
                    <div class="new-update clearfix">
                        <div class="update-done"><a title="" href=""><strong>Libur Tahun Baru</strong></a> <span>Selamat tahun baru 2023. Semoga tahun ini lebih baik dari tahun sebelumnya.</span> </div>
                        <div class="update-date"><span class="update-day">1</span>January</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>

<!--======================================================================-->
<script>
    const labels = [
        'January',
        'February',
        'March',
        'April',
        'May',
        'June',
        'July',
        'August',
        'September',
        'October',
        'November',
        'December',
    ];

    const data = {
        labels: labels,
        datasets: [{
            label: 'Pengunjung',
            backgroundColor: 'rgb(255, 99, 132)',
            borderColor: 'rgb(255, 99, 132)',
            data: [0, 3, 5, 1, 7, 10, 4, 7, 5, 15, 3, 7],
        }]
    };

    const config = {
        type: 'line',
        data: data,
        options: {}
    };
</script>
<!--======================================================================-->

<script>
    const orderChart = new Chart(
        document.getElementById('orderChart'),
        config
    );
</script>

<!--======================================================================-->
<script src="<?= base_url('assets/'); ?>js/admin/excanvas.min.js"></script>
<script src="<?= base_url('assets/'); ?>js/admin/jquery.min.js"></script>
<script src="<?= base_url('assets/'); ?>js/admin/jquery.ui.custom.js"></script>
<script src="<?= base_url('assets/'); ?>js/admin/bootstrap.min.js"></script>
<script src="<?= base_url('assets/'); ?>js/admin/jquery.flot.min.js"></script>
<script src="<?= base_url('assets/'); ?>js/admin/jquery.flot.resize.min.js"></script>
<script src="<?= base_url('assets/'); ?>js/admin/jquery.peity.min.js"></script>
<script src="<?= base_url('assets/'); ?>js/admin/fullcalendar.min.js"></script>
<script src="<?= base_url('assets/'); ?>js/admin/maruti.js"></script>
<script src="<?= base_url('assets/'); ?>js/admin/maruti.dashboard.js"></script>
<script src="<?= base_url('assets/'); ?>js/admin/maruti.chat.js"></script>


<script type="text/javascript">
    // This function is called from the pop-up menus to transfer to
    // a different page. Ignore if the value returned is a null string:
    function goPage(newURL) {

        // if url is empty, skip the menu dividers and reset the menu selection to default
        if (newURL != "") {

            // if url is "-", it is this page -- reset the menu:
            if (newURL == "-") {
                resetMenu();
            }
            // else, send page to designated URL            
            else {
                document.location.href = newURL;
            }
        }
    }

    // resets the menu selection upon entry to this page:
    function resetMenu() {
        document.gomenu.selector.selectedIndex = 2;
    }
</script>