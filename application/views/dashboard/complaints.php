<div id="content">
    <div id="content-header">
        <div id="breadcrumb"> <a href="<?= base_url('admin/dashboard'); ?>" title="Go to Dashboard" class="tip-bottom"><i class="icon-home"></i> Dashboard</a> / <a href="<?= base_url('admin/members'); ?>" class="current">Complaints</a> </div>
        <h1>Complaints</h1>
    </div>
    <div class="container-fluid">
        <div class="row-fluid">
            <div class="span12">
                <div class="widget-box">
                    <div class="widget-title">
                        <h5>Complaints Data</h5>
                    </div>
                    <div class="widget-content nopadding">
                        <table class="table table-bordered data-table">
                            <thead>
                                <tr>
                                    <th scope="col">Name</th>
                                    <th scope="col">Email</th>
                                    <th scope="col">Phone</th>
                                    <th scope="col">Complaint</th>
                                    <th scope="col">Lampiran</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                foreach ($complaint as $complaint) : ?>
                                    <tr>
                                        <td><?= $complaint['nama']; ?></td>
                                        <td style="text-align: center;"><?= $complaint['email']; ?></td>
                                        <td style="text-align: center;"><?= $complaint['no_hp']; ?></td>
                                        <td><?= $complaint['isi']; ?></td>
                                        <td>
                                            <?php
                                                if($complaint['lampiran']){
                                                    $arr = explode(".", $complaint['lampiran']);
                                                    $ext = $arr[count($arr)-1];
                                                    $img_arr = ['jpg', 'png', 'jpeg'];
                                                    $vid_arr = ['mp4', '3gp', 'mov', 'webm'];
                                                    $is_img = in_array($ext, $img_arr);
                                                    $is_vid = in_array($ext, $vid_arr);
                                                    $url = base_url('assets/pengaduan/') . $complaint['lampiran'];
                                                    if($is_img){
                                                        echo '<img src="'.$url.'" width="200">';
                                                    }
                                                    if($is_vid){
                                                        echo '<video width="200" height="200" controls>
                                                            <source src="'.$url.'" type="video/'.$ext.'">
                                                            Your browser does not support the video tag.
                                                        </video>';
                                                    }
                                                }else{
                                                    echo "-";
                                                }
                                            ?>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
<script src="<?= base_url('assets/'); ?>js/admin/jquery.min.js"></script>
<script src="<?= base_url('assets/'); ?>js/admin/jquery.ui.custom.js"></script>
<script src="<?= base_url('assets/'); ?>js/admin/bootstrap.min.js"></script>
<script src="<?= base_url('assets/'); ?>js/admin/jquery.uniform.js"></script>
<script src="<?= base_url('assets/'); ?>js/admin/select2.min.js"></script>
<script src="<?= base_url('assets/'); ?>js/admin/jquery.dataTables.min.js"></script>
<script src="<?= base_url('assets/'); ?>js/admin/maruti.js"></script>
<script src="<?= base_url('assets/'); ?>js/admin/maruti.tables.js"></script>