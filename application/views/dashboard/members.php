<div id="content">
    <div id="content-header">
        <div id="breadcrumb"> <a href="<?= base_url('admin/dashboard'); ?>" title="Go to Dashboard" class="tip-bottom"><i class="icon-home"></i> Dashboard</a> / <a href="<?= base_url('admin/members'); ?>" class="current">Members</a> </div>
        <h1>Members</h1>
    </div>
    <div class="container-fluid">
        <div class="row-fluid">
            <div class="span12">
                <div class="widget-box">
                    <div class="widget-title">
                        <h5>Members Data</h5>
                    </div>
                    <div class="widget-content nopadding">
                        <table class="table table-bordered data-table">
                            <thead>
                                <tr>
                                    <th scope="col">Name</th>
                                    <th scope="col">Email</th>
                                    <th scope="col">Role</th>
                                    <th scope="col">Activation</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                foreach ($user as $usr) : ?>
                                    <tr>
                                        <td><?= $usr['nama']; ?></td>
                                        <td style="text-align: center;"><?= $usr['email']; ?></td>
                                        <td style="text-align: center;"><?php if ($usr['role_id'] == 1) {
                                                                            echo 'Administrator';
                                                                        } else {
                                                                            echo 'Member';
                                                                        } ?></td>
                                        <td style="text-align: center;"><input class="form-control" type="number" style="text-align: center;" min="0" max="1" value="<?= $usr['is_active']; ?>"></td>
                                    </tr>
                                <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
<script src="<?= base_url('assets/'); ?>js/admin/jquery.min.js"></script>
<script src="<?= base_url('assets/'); ?>js/admin/jquery.ui.custom.js"></script>
<script src="<?= base_url('assets/'); ?>js/admin/bootstrap.min.js"></script>
<script src="<?= base_url('assets/'); ?>js/admin/jquery.uniform.js"></script>
<script src="<?= base_url('assets/'); ?>js/admin/select2.min.js"></script>
<script src="<?= base_url('assets/'); ?>js/admin/jquery.dataTables.min.js"></script>
<script src="<?= base_url('assets/'); ?>js/admin/maruti.js"></script>
<script src="<?= base_url('assets/'); ?>js/admin/maruti.tables.js"></script>