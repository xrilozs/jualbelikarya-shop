<div id="content">
    <div id="content-header">
        <div id="breadcrumb"> <a href="<?= base_url('admin/dashboard'); ?>" title="Go to Dashboard" class="tip-bottom"><i class="icon-home"></i> Dashboard</a> / <a href="<?= base_url('admin/products'); ?>" class="current">Products</a> </div>
        <h1>Product</h1>
    </div>
    <div class="container-fluid">
        <div class="row-fluid">
            <div class="widget-box">
                <div class="widget-title">
                    <h5>Edit Product</h5>
                </div>
                <div class="widget-content nopadding">
                    <?php foreach ($product as $p) { ?>
                        <form action="<?= base_url('admin/ubahProduk'); ?>" method="post" enctype="multipart/form-data">
                            <div class="span2">
                                <div class="form-group">
                                    <?php
                                    if (isset($p['image_produk'])) { ?>

                                        <input type="hidden" name="old_pict" id="old_pict" value="<?= $p['image_produk']; ?>">

                                        <picture>
                                            <source srcset="" type="image/svg+xml">
                                            <img src="<?= base_url('assets/img/upload/') . $p['image_produk']; ?>" height="200" width="200" alt="...">
                                        </picture>

                                    <?php } ?>
                                </div>
                            </div>
                            <div class="span10">
                                <input type="file" class="form-control form-control-user span12" name="image_produk" id="image_produk">
                                <div class="form-group">
                                    <input type="hidden" name="id_produk" id="id_produk" value="<?php echo $p['id_produk']; ?>">
                                    <input type="text" class="form-control form-control-user span10" id="nama_produk" name="nama_produk" placeholder="Masukkan nama produk" value="<?= $p['nama_produk']; ?>">
                                </div>
                                <div class="form-group">
                                    <div class="input-prepend span10">
                                        <span class="add-on">Rp</span>
                                        <input type="text" class="form-control form-control-user span10" name="harga" id="harga" placeholder="Masukkan harga produk" value="<?= $p['harga']; ?>">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <input type="text" class="form-control form-control-user span10" id="desc" name="desc" rows="3" maxlength="250" placeholder="Masukkan deskripsi produk" value="<?= $p['deskripsi']; ?>"></input>
                                </div>
                                <div class="form-group">
                                    <select name="objek" id="objek" class="form-control form-control-user span10">
                                        <option value="<?= $p['objek']; ?>" selected disabled><?= $p['objek']; ?></option>
                                        <option value="Hewan">Hewan</option>
                                        <option value="Tumbuhan">Tumbuhan</option>
                                        <option value="Manusia">Manusia</option>
                                    </select>
                                </div>
                            </div>
                            <div style="text-align: center;">
                                <button type="submit" class="btn btn-success">Save</button>
                            </div>
                        </form>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="<?= base_url('assets/'); ?>js/admin/jquery.min.js"></script>
<script src="<?= base_url('assets/'); ?>js/admin/jquery.ui.custom.js"></script>
<script src="<?= base_url('assets/'); ?>js/admin/bootstrap.min.js"></script>
<script src="<?= base_url('assets/'); ?>js/admin/jquery.uniform.js"></script>
<script src="<?= base_url('assets/'); ?>js/admin/select2.min.js"></script>
<script src="<?= base_url('assets/'); ?>js/admin/jquery.dataTables.min.js"></script>
<script src="<?= base_url('assets/'); ?>js/admin/maruti.js"></script>
<script src="<?= base_url('assets/'); ?>js/admin/maruti.tables.js"></script>