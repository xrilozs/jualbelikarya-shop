<div id="content">
    <div id="content-header">
        <div id="breadcrumb"> <a href="<?= base_url('admin/dashboard'); ?>" title="Go to Dashboard" class="tip-bottom"><i class="icon-home"></i> Dashboard</a> / <a href="<?= base_url('admin/members'); ?>" class="current">Orders</a> </div>
        <h1>Orders</h1>
    </div>
    <div class="container-fluid">
        <div class="row-fluid">
            <div class="span12">
                <div class="widget-box">
                    <div class="widget-title">
                        <h5>Orders Data</h5>
                    </div>
                    <div class="widget-content nopadding">
                        <table class="table table-bordered data-table">
                            <thead>
                                <tr>
                                    <th scope="col">Date</th>
                                    <th scope="col">Name</th>
                                    <th scope="col">Email</th>
                                    <th scope="col">Phone</th>
                                    <th scope="col">Orders</th>
                                    <th scope="col">Address</th>
                                    <th scope="col">Notes</th>
                                    <th scope="col">Transaction</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($order as $odr) : ?>
                                    <tr>
                                        <td><?= $odr['tgl_order']; ?></td>
                                        <td><?= $odr['nama']; ?></td>
                                        <td style="color: dodgerblue;"><?= $odr['email']; ?></td>
                                        <td><?= $odr['no_hp']; ?></td>
                                        <td><?= $odr['orders']; ?></td>
                                        <td><?= $odr['alamat']; ?></td>
                                        <td><?= $odr['catatan']; ?></td>
                                        <td style="color: forestgreen;"><?= $odr['transaction']; ?></td>
                                    </tr>
                                <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
<script src="<?= base_url('assets/'); ?>js/admin/jquery.min.js"></script>
<script src="<?= base_url('assets/'); ?>js/admin/jquery.ui.custom.js"></script>
<script src="<?= base_url('assets/'); ?>js/admin/bootstrap.min.js"></script>
<script src="<?= base_url('assets/'); ?>js/admin/jquery.uniform.js"></script>
<script src="<?= base_url('assets/'); ?>js/admin/select2.min.js"></script>
<script src="<?= base_url('assets/'); ?>js/admin/jquery.dataTables.min.js"></script>
<script src="<?= base_url('assets/'); ?>js/admin/maruti.js"></script>
<script src="<?= base_url('assets/'); ?>js/admin/maruti.tables.js"></script>