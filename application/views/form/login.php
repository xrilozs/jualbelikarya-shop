<!DOCTYPE html>
<html lang="en">

<head>
    <title>Login Page</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!--===============================================================================================-->
    <link rel="icon" type="image/png" href="<?= base_url('assets/'); ?>icons/login/login_icon.png">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="<?= base_url('assets/'); ?>vendor/login/bootstrap/css/bootstrap.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="<?= base_url('assets/'); ?>vendor/login/animate/animate.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="<?= base_url('assets/'); ?>vendor/login/css-hamburgers/hamburgers.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="<?= base_url('assets/'); ?>vendor/login/animsition/css/animsition.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="<?= base_url('assets/'); ?>vendor/login/select2/select2.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="<?= base_url('assets/'); ?>vendor/login/daterangepicker/daterangepicker.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="<?= base_url('assets/'); ?>css/login/main.css">
    <!--===============================================================================================-->
</head>

<body style="background-color: #666666;">

    <div class="limiter">
        <div class="container-login100">
            <div class="wrap-login100">
                <form class="login100-form validate-form" method="post" action="<?= base_url('Login'); ?>">
                    <div class="login100">
                        <?= $this->session->flashdata('pesan'); ?>
                    </div>

                    <span class="login100-form-title p-b-43">
                        Login
                    </span>

                    <div class="wrap-input100 validate-input" data-validate="Email belum diisi" style="margin: 15px 0px 15px 0px;">
                        <input class="input100" type="text" id="email" name="email">
                        <span class="focus-input100"></span>
                        <span class="label-input100">Email</span>
                    </div>
                    <?= form_error('email', '<small class="text-danger pl-3">', '</small>'); ?>

                    <div class="wrap-input100 validate-input" data-validate="Password belum diisi" style="margin: 15px 0px 15px 0px;">
                        <input class="input100" type="password" id="password" name="password">
                        <span class="focus-input100"></span>
                        <span class="label-input100">Password</span>
                    </div>
                    <?= form_error('password', '<small class="text-danger pl-3">', '</small>'); ?>

                    <div class="container-login100-form-btn">
                        <button type="submit" class="login100-form-btn">
                            <b>Masuk</b>
                        </button>
                    </div>

                    <div class="flex-sb-m w-full p-t-3 p-b-32">
                        <div style="margin: 15px 0px 0px 0px; text-align: center;">
                            <a href="" class="txt1" data-toggle="modal" data-target="#Register">
                                Belum menjadi Member? Daftar Akun Sekarang
                            </a>
                        </div>
                    </div>
                </form>

                <div id="Register" class="modal hide">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title">Daftar Akun Baru</h5>
                                <button data-dismiss="modal" class="close" title="close" type="button">×</button>
                            </div>
                            <form action="<?= base_url('login/registrasi'); ?>" method="post" enctype="multipart/form-data">
                                <div class="modal-body">
                                    <div class="form-group">
                                        <div class="form-control validate-input" data-validate="Nama Lengkap belum diisi">
                                            <input type="text" class="input-form form-control:focus" id="nama" name="nama" placeholder="Nama Lengkap" title="Isikan dengan menggunakan nama lengkap anda" required>
                                            <?= form_error('nama', '<small class="text-danger pl-3">', '</small>'); ?>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="form-control validate-input" data-validate="Email belum diisi">
                                            <input type="email" class="input-form form-control:focus" id="email" name="email" placeholder="Email" title="Bentuk : example@email.com" required>
                                            <?= form_error('email', '<small class="text-danger pl-3">', '</small>'); ?>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="form-control validate-input" data-validate="Password belum diisi">
                                            <input type="password" class="input-form form-control:focus" id="password" name="password" placeholder="Password" title="Harap membuat password yang kuat" required>
                                            <?= form_error('password', '<small class="text-danger pl-3">', '</small>'); ?>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <input type="checkbox" title="Centang kotak apabila umur anda sudah diatas 18 thn" required><a class="ml-2" id="syarat" onclick="aturan()" href="<?= base_url('service/terms'); ?>#syarat">Anda berusia diatas 18 tahun</a>
                                    </div>
                                </div>
                                <div class="modal-footer"> <button type="submit" class="btn btn-success">Konfirmasi</button> <button type="reset" class="btn">Batal</button> </div>
                            </form>
                        </div>
                    </div>
                </div>

                <img class="login100-more" src="https://i.pinimg.com/originals/c1/f1/a6/c1f1a67d15b620f5e18a48c7b2c90caf.gif" alt="<?= base_url('assets/'); ?>img/login_img.gif">
            </div>
        </div>
    </div>

    <script>
        $('.alert-message').alert().delay(1500).slideUp('slow');
    </script>
    <!--===============================================================================================-->
    <script src="<?= base_url('assets/'); ?>vendor/login/jquery/jquery-3.2.1.min.js"></script>
    <!--===============================================================================================-->
    <script src="<?= base_url('assets/'); ?>vendor/login/animsition/js/animsition.min.js"></script>
    <!--===============================================================================================-->
    <script src="<?= base_url('assets/'); ?>vendor/login/bootstrap/js/popper.js"></script>
    <script src="<?= base_url('assets/'); ?>vendor/login/bootstrap/js/bootstrap.min.js"></script>
    <!--===============================================================================================-->
    <script src="<?= base_url('assets/'); ?>vendor/login/select2/select2.min.js"></script>
    <!--===============================================================================================-->
    <script src="<?= base_url('assets/'); ?>vendor/login/daterangepicker/moment.min.js"></script>
    <script src="<?= base_url('assets/'); ?>vendor/login/daterangepicker/daterangepicker.js"></script>
    <!--===============================================================================================-->
    <script src="<?= base_url('assets/'); ?>vendor/login/countdowntime/countdowntime.js"></script>
    <!--===============================================================================================-->
    <script src="<?= base_url('assets/'); ?>js/login/main.js"></script>

</body>

</html>