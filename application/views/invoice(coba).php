<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="<?= base_url('assets/'); ?>css/invoice.css">
    <link rel="stylesheet" href="<?= base_url('assets/'); ?>css/invoice/custom.css">
    <link rel="stylesheet" href="<?= base_url('assets/'); ?>css/invoice/bootstrap.css">
    <link rel="shortcut icon" href="<?= base_url('assets/'); ?>icons/invoice/invoice.png">
    <title>Invoice Pembelian</title>
</head>

<body>
    <div class="receipt-content">
        <div class="container bootstrap snippets bootdey">
            <div class="row">
                <div class="col-md-12">
                    <div class="invoice-wrapper">

                        <div class="payment-info mt-auto">
                            <div class="row">
                                <div class="col-sm-6">
                                    <span>INVOICE</span>
                                    <strong>2532212413</strong>
                                </div>
                                <div class="col-sm-6 text-right">
                                    <span>Tanggal Pembelian</span>
                                    <strong>
                                        <?php date_default_timezone_set('Asia/Jakarta');
                                        echo date('j M Y, H:i'); ?>
                                    </strong>
                                </div>
                            </div>
                        </div>

                        <div class="payment-details">
                            <div class="row">
                                <div class="col-sm-6">
                                    <span class="mb-2">Penjual</span>
                                    <strong>
                                        Jual Beli Karya
                                    </strong>
                                    <p>
                                        Bogor, Jawa Barat<br>
                                        Indonesia
                                    </p>
                                </div>
                                <div class="col-sm-6 text-right">
                                    <span class="mb-2">Pembeli</span>
                                    <strong>
                                        <?= $fstname . " " . $endname; ?>
                                    </strong>
                                    <ul class="list-unstyled">
                                        <li><?= $alamat; ?></li>
                                        <li><?= $kota; ?></li>
                                        <li><?= $negara; ?></li>
                                    </ul>
                                    <a href="#">
                                        <?= $email; ?>
                                    </a>
                                </div>
                            </div>
                        </div>

                        <div class="line-items">
                            <div class="headers clearfix">
                                <div class="row">
                                    <div class="col-3 mr-auto">Nama Barang</div>
                                    <div class="col-3">Banyaknya</div>
                                    <div class="col-3 text-right">Jumlah</div>
                                </div>
                            </div>
                            <div class="items">
                                <div class="row item">
                                    <?php foreach ($cart as $troli) { ?>
                                        <div class="col-3 mr-auto desc">
                                            <?= $troli['nama_produk']; ?>
                                        </div>
                                        <div class="col-3 qty">
                                            1
                                        </div>
                                        <div class="col-3 amount text-right">
                                            <?= "Rp " . number_format($troli['harga'], 0, '', '.'); ?>
                                        </div>
                                    <?php } ?>
                                </div>
                            </div>
                            <div class="total text-right">
                                <p class="extra-notes">
                                    <strong>Catatan</strong>
                                    <?= $catatan; ?>
                                </p>
                                <div class="field">
                                    Subtotal <span><?= "Rp " . number_format($subtotal, 0, '', '.'); ?></span>
                                </div>
                                <div class="field">
                                    Biaya pengiriman <span><?= $ongkir; ?></span>
                                </div>
                                <div class="field">
                                    Diskon <span><?= $diskon; ?></span>
                                </div>
                                <div class="field">
                                    PPN <span><?= $ppn; ?></span>
                                </div>
                                <div class="field grand-total">
                                    Total <span><?= "Rp " . number_format($total, 0, '', '.'); ?></span>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="footer">
                        Copyright &copy; 2023. JualBeliKarya
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>
        window.print();
    </script>

</body>

</html>