<div class="container">
    <!-- HERO SECTION-->
    <section class="py-5 bg-light">
        <div class="container">
            <div class="row px-4 px-lg-5 py-lg-4 align-items-center">
                <div class="col-lg-6">
                    <h1 class="h2 text-uppercase mb-0">Pusat Bantuan</h1>
                </div>
            </div>
        </div>
    </section>
    <section class="py-4">
        <h3 class="mb-4">Pertanyaan dan Jawaban</h3>
        <ul class="list-unstyled">
            <li class="form-text bg-light"><b>Bagaimana cara membuat akun ?</b></li>
            <li class="form-text ml-5 mb-3">Menuju ke halaman login, kemudian pilih buat akun</li>
            <li class="form-text bg-light"><b>Bagaimana cara menambahkan barang ke troli ?</b></li>
            <li class="form-text ml-5">Pilih barang yang ingin dibeli, kemudian pilih tombol "Tambahkan ke troli"</li>
        </ul>
    </section>
    <section class="py-2">
        <h3 class="mb-3">Hubungi Kami</h3>
        <p>Untuk mendapatkan bantuan atau informasi lebih lanjut, silahkan hubungi kami di:</p>
        <ul class="list-unstyled">
            <li class="form-text"><b>Jual Beli Karya</b></li>
            <li class="form-text">Bogor, Jawa Barat</li>
            <li class="form-text mb-3">Indonesia</li>
            <li class="form-text">081234567890</li>
            <li class="form-text">Fax : 1220055</li>
            <li class="form-text"><a href="">callcenter@jualbelikarya.com</a></li>
        </ul>
    </section>
</div>