<div class="container">
    <!-- HERO SECTION-->
    <section class="py-5 bg-light">
        <div class="container">
            <div class="row px-4 px-lg-5 py-lg-4 align-items-center">
                <div class="col-lg-6">
                    <h1 class="h2 text-uppercase mb-0">Layanan Pengaduan</h1>
                </div>
            </div>
        </div>
    </section>
    <?= $this->session->flashdata('pesan'); ?>
    <section class="py-5">
        <form class="validate-form" action="<?= base_url('service/add_complaint'); ?>" method="post" enctype="multipart/form-data">
        <div class="col-lg-12 mb-4">
            <h2 class="h5 text-uppercase mb-4" style="text-align: center;">Riwayat Pesanan</h2>
            <select class="form-control" name="id_order" style="text-align: center;">
                <option active disabled>Pilih pesanan yang ingin diajukan komplain</option>
                <?php foreach ($order as $data) : ?>
                    <?php
                        $cart = json_decode($data['orders'], true);
                        $orders = "[" . date("d M Y", strtotime($data['tgl_order'])) . "] ";
                        for($i=0; $i<count($cart); $i++) {
                            $orders .= $cart[$i]['nama_produk'];
                            if($i != count($cart)-1){
                                $orders .= ", ";
                            }
                        }    
                    ?>
                    <option value="<?=$data['id_order'];?>"><?= $orders; ?></option>
                <?php endforeach; ?>
            </select>
        </div>

        <!-- Form Pengaduan-->
        <?php $disabaled_str = count($order) > 0 ? '' : 'disabled'; ?>
        <h2 class="h5 text-uppercase mb-4" style="text-align: center;">Form Pengaduan</h2>
            <div class="col-lg-12">
                <div class="col-lg-12 form-group">
                    <label class="text-small text-uppercase" for="nama">Nama Pelanggan</label>
                    <input class="form-control form-control-lg" name="nama" id="nama" type="text" placeholder="Masukkan Nama Anda" required <?=$disabaled_str;?>>
                </div>

                <div class="col-lg-12 form-group">
                    <label class="text-small text-uppercase" for="email">Alamat Email</label>
                    <input class="form-control form-control-lg" name="email" id="email" type="email" placeholder="cont : email@example.com" required <?=$disabaled_str;?>>
                </div>
                <?= form_error('email', '<small class="text-danger pl-3">', '</small>'); ?>

                <div class="col-lg-12 form-group">
                    <label class="text-small text-uppercase" for="no_hp">Nomor Handphone</label>
                    <input class="form-control form-control-lg" name="no_hp" id="no_hp" type="tel" placeholder="cont : +62 123456789" required <?=$disabaled_str;?>>
                </div>
                <?= form_error('nohp', '<small class="text-danger pl-3">', '</small>'); ?>
                <div class="col-lg-12 form-group">
                    <label class="text-small text-uppercase" for="complaint">Isi Pengaduan</label>
                    <textarea class="form-control form-control-lg" name="complaint" id="complaint" cols="38" rows="3" maxlength="250" placeholder="Tuliskan Pengaduan Anda ..." required <?=$disabaled_str;?>></textarea>
                </div>
                <div class="col-lg-6 form-group">
                    <label class="text-small text-uppercase" for="attach">Lampirkan gambar/video</label>
                    <input type="file" name="attach" id="attach" <?=$disabaled_str;?>>
                </div>
                <div class="col-lg-12 form-group py-4" style="text-align: center;">
                    <button class="btn btn-dark mr-2" type="submit" <?=$disabaled_str;?>>Konfirmasi</button> 
                    <button class="btn btn-danger" type="reset" <?=$disabaled_str;?>>Reset</button>
                </div>
            </div>
        </form>
    </section>
</div>