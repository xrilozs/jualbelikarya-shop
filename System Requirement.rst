#####################
Apa itu CodeIgniter
#####################

    CodeIgniter adalah Kerangka Pengembangan Aplikasi - toolkit - untuk developer
yang membangun sebuah situs web menggunakan PHP. Tujuannya adalah untuk mengembangkan proyek
jauh lebih cepat daripada jika menulis kode dari awal, dengan menyediakan satu set perpustakaan
untuk tugas-tugas yang biasa dibutuhkan, serta yang sederhana membuat antarmuka dan struktur logis
untuk mengakses perpustakaan ini.
    CodeIgniter memungkinkan Anda secara kreatif fokus pada proyek Anda dengan meminimalkan
jumlah kode yang dibutuhkan untuk tugas yang diberikan.

*******************
Persyaratan Server
*******************

✅ PHP versi 5.6 atau 7.4
✅ XAMPP versi 7.4.28
✅ Apache versi 2.4.53

Website ini seharusnya bekerja pada PHP versi 5.6, akan tetapi kami menyarankan untuk menggunakan
PHP versi baru yaitu 7.4 dengan Apache versi 2.4.53.

***********
Sumber daya
***********

- `Panduan Pengguna <https://codeigniter.com/docs>`_
- `Terjemahan File Bahasa <https://github.com/bcit-ci/codeigniter3-translations>`_
- `Forum Komunitas <http://forum.codeigniter.com/>`_
- `Wiki Komunitas <https://github.com/bcit-ci/CodeIgniter/wiki>`_